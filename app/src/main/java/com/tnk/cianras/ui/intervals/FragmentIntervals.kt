package com.tnk.cianras.ui.intervals

import android.app.TimePickerDialog
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.tnk.cianras.R
import com.tnk.cianras.database.intervals.Interval
import com.tnk.cianras.database.intervals.ListAdapter_Intervals
import com.tnk.cianras.database.intervals.DataIntervals
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlinx.android.synthetic.main.obj_current_interval.*

class FragmentIntervals : Fragment() {

    private val TAG: String = "FragIntervals ::"

    private lateinit var btn_add: Button
    private lateinit var btn_done: Button

    private lateinit var tv_currentTime: TextView
    private lateinit var tv_ci_type: TextView
    private lateinit var tv_ci_time: TextView
    private lateinit var tv_ci_duration: TextView

    private lateinit var lvIntervals: ListView
    private lateinit var laIntervals: ListAdapter_Intervals

    private var dataIntervals: DataIntervals = DataIntervals

    companion object {

        fun newInstance(): FragmentIntervals {
            return FragmentIntervals()
        }
    }

    init {
        Log.v(TAG, "init start")


        Log.v(TAG, "init complete")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.frag_intervals, container, false)

        initUI(root)

        return root
    }

    /**
     *
     */
    private fun initUI(root: View) {

        // @LABEL ListView
        laIntervals = ListAdapter_Intervals(
            requireContext(),
            dataIntervals.mIntervalList
        )
        lvIntervals = root.findViewById<ListView>(R.id.lv_intervals)
        lvIntervals.adapter = laIntervals

        val spinner = root.findViewById<Spinner>(R.id.spinner_interval_type)
        var currentType: DataIntervals.INTERVALTYPES = DataIntervals.INTERVALTYPES.REST
        if (spinner != null) {
            val adapter = ArrayAdapter(
                requireContext(),
                android.R.layout.simple_spinner_item, Arrays.toString(
                    DataIntervals.INTERVALTYPES.values()
                ).replace("^.|.$[]", "").split(", ")
            )
            spinner.adapter = adapter
        }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                currentType = DataIntervals.INTERVALTYPES.values()[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                currentType = DataIntervals.INTERVALTYPES.REST
            }
        }

        btn_add = root.findViewById(R.id.button_add_interval)
        btn_add.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                var interval = Interval(
                    currentType,
                    10000
                )
                dataIntervals.mIntervalList.add(interval)
                //dataIntervals.mIntervalList.add(interval)
                laIntervals.notifyDataSetChanged()
            }
        })

        btn_done = root.findViewById(R.id.button_start_intervals)
        btn_done.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.v(TAG, "DONE!")
                if(!dataIntervals.TIMING) {
                    dataIntervals.TIMING = true
                    loadIntervals()
                    startInterval()
                }

            }
        } )

        val cal = Calendar.getInstance()
        tv_currentTime = root.findViewById(R.id.tv_ct)

        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, minute, second ->
            //cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)
            cal.set(Calendar.SECOND, second)

            tv_currentTime.text = SimpleDateFormat("mm:ss").format(cal.time)
        }

        tv_currentTime.setOnClickListener {
            TimePickerDialog(
                context,
                timeSetListener,
                cal.get(Calendar.MINUTE),
                cal.get(Calendar.SECOND),
                true
            ).show()
        }

        tv_ci_duration = root.findViewById(R.id.tv_ci_duration)
        tv_ci_time = root.findViewById(R.id.tv_ci_time)
        tv_ci_type = root.findViewById(R.id.tv_ci_type)

    }

    /**
     *
     */
    private fun loadIntervals() {
        // refresh the countdown array
        dataIntervals.mCountdownArray = ArrayList<CountDownTimer>()

        for (item in dataIntervals.mIntervalList) {
            val timer = object : CountDownTimer(item.intervalDuration, 1000) {
                override fun onFinish() {
                    Log.v(TAG, "Timer Complete ${dataIntervals.mCountDownArrayPoint}")

                    dataIntervals.mCountDownArrayPoint++
                    if(dataIntervals.mCountDownArrayPoint == dataIntervals.mIntervalList.size)  {
                        dataIntervals.mCountDownArrayPoint = 0
                        dataIntervals.TIMING = false
                    } else {
                        startInterval()
                    }
                }

                override fun onTick(millisUntilFinished: Long) {
                    updateCurrentInterval(millisUntilFinished)

                    Log.v(TAG, millisUntilFinished.toString() + "s in " + item.intervalType)
                }
            }
            dataIntervals.mCountdownArray.add(timer)
        }
        Log.v(TAG, "Loaded ${dataIntervals.mCountdownArray.size} CountDownTimers!")
    }

    /**
     *
     */
    private fun updateCurrentInterval(millisUntilFinished: Long) {

        tv_ci_type.text = dataIntervals.mIntervalList[dataIntervals.mCountDownArrayPoint].intervalType.toString()
        tv_ci_duration.text = dataIntervals.millisToString(
            dataIntervals.mIntervalList[dataIntervals.mCountDownArrayPoint].intervalDuration)
        tv_currentTime.text = dataIntervals.millisToString(millisUntilFinished)
        tv_ci_time.text = dataIntervals.millisToString(millisUntilFinished)

        activity?.tv_ci_id?.text = (dataIntervals.mCountDownArrayPoint+1).toString()
        activity?.tv_ci_total?.text = dataIntervals.mCountdownArray.size.toString()

    }

    /**
     *
     */
    private fun startInterval() {
        dataIntervals.mCountdownArray[dataIntervals.mCountDownArrayPoint].start()
    }

    /**
     *
     */
    var runnerCountdown: Runnable = object : Runnable {
        override fun run() {
            val timer = object : CountDownTimer(5000, 1000) {
                override fun onFinish() {
                    TODO("Not yet implemented")
                }

                override fun onTick(millisUntilFinished: Long) {
                    TODO("Not yet implemented")
                }
            }

        }

    }

}