package com.tnk.cianras.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.tnk.cianras.MainActivity
import com.tnk.cianras.R
import com.tnk.cianras.database.intervals.DataIntervals
import com.tnk.cianras.database.location.DataLocation
import com.xw.repo.BubbleSeekBar
import com.xw.repo.BubbleSeekBar.OnProgressChangedListenerAdapter
import org.jetbrains.anko.enabled


class FragmentHome : Fragment() {

    private val TAG: String = "FragmentHome :: "
    private var tv_distance: TextView? = null

    private var dataIntervals: DataIntervals = DataIntervals
    private var dataLocation: DataLocation = DataLocation
    var btn_start: Button? = null
    var btn_pause: Button? = null

    companion object {
        fun newInstance() = FragmentHome()

        var seek_distance: BubbleSeekBar? = null
        //var seek_progress: BubbleSeekBar? = null

    }

    private lateinit var viewModel: FragmentHomeVM

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root =  inflater.inflate(R.layout.frag_home, container, false)

        initUI(root)
        //
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FragmentHomeVM::class.java)
        // TODO: Use the ViewModel
    }

    /**
     * @LABEL LIFECYCLE
     */
    override fun onDetach() {
        super.onDetach()
        Log.v(TAG, "onDetach()")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.v(TAG, "onDestroyView()")
    }

    /**
     * Initialize the User Interface for the Minimal Interface
     */
    private fun initUI(root: View) {
        tv_distance = root.findViewById(R.id.tv_race_distance)

        btn_pause = root.findViewById(R.id.button_pause)
        btn_start = root.findViewById(R.id.button_start)
        btn_start?.enabled = false
        btn_start?.text = getString(R.string.btn_start)
        btn_pause?.text = getString(R.string.btn_pause)

        // FIXME Consider adding a confirmation popup
        btn_start?.setOnClickListener {
            if (!dataLocation.RACE_STARTED) {
                startRace()
            } else {
                btn_pause?.enabled = false
                dataLocation.RACE_STARTED = false
                btn_start?.text = (getString(R.string.btn_start))
                Toast.makeText(
                    requireContext(),
                    getString(R.string.race_stopped),
                    Toast.LENGTH_LONG
                )
                    .show()
            }
        }

        btn_pause?.setOnClickListener {
            if (!dataLocation.RACE_PAUSED) {
                dataLocation.RACE_PAUSED = true
                btn_pause?.text = (getString(R.string.btn_resume))
                Toast.makeText(
                    requireContext(),
                    getString(R.string.race_paused),
                    Toast.LENGTH_LONG
                )
                    .show()
            } else {
                dataLocation.RACE_PAUSED = false
                btn_pause?.text = (getString(R.string.btn_pause))
                Toast.makeText(
                    requireContext(),
                    getString(R.string.race_resumed),
                    Toast.LENGTH_LONG
                )
                    .show()
            }
        }

        // @LABEL Bubble SeekBars!
        val progress: Float = if (dataLocation.RACE_STARTED) DataLocation.mRaceDistance else 0.0f
        seek_distance = root.findViewById(R.id.bubseek_distance)
        seek_distance?.configBuilder
            ?.min(0.0f)
            ?.max(2000f)
            ?.progress(progress)
            ?.sectionCount(10)
            ?.trackColor(ContextCompat.getColor(requireContext(), R.color.color_gray))
            ?.secondTrackColor(ContextCompat.getColor(requireContext(), R.color.color_blue))
            ?.thumbColor(ContextCompat.getColor(requireContext(), R.color.color_blue))
            ?.showSectionText()
            ?.sectionTextColor(ContextCompat.getColor(requireContext(), R.color.colorPrimary))
            ?.sectionTextSize(18)
            ?.showThumbText()
            ?.thumbTextColor(ContextCompat.getColor(requireContext(), R.color.color_red))
            ?.thumbTextSize(18)
            ?.bubbleColor(ContextCompat.getColor(requireContext(), R.color.color_green))
            ?.bubbleTextSize(18)
            ?.showSectionMark()
            //?.seekBySection()
            ?.touchToSeek()
            //?.autoAdjustSectionMark()
            ?.sectionTextPosition(BubbleSeekBar.TextPosition.BOTTOM_SIDES)
            //?.showProgressInFloat()
            ?.build()

        seek_distance?.onProgressChangedListener = object : OnProgressChangedListenerAdapter() {
            override fun onProgressChanged(
                bubbleSeekBar: BubbleSeekBar,
                progress: Int,
                progressFloat: Float,
                fromUser: Boolean
            ) {
                // Do something while the SeekBar is being interacted
                tv_distance?.text = "Distance : $progress m"

                val color: Int
                color = if (progress <= 10) {
                    ContextCompat.getColor(requireContext(), R.color.color_red)
                } else if (progress <= 40) {
                    ContextCompat.getColor(requireContext(), R.color.color_gray)
                } else if (progress <= 70) {
                    ContextCompat.getColor(requireContext(), R.color.colorAccent)
                } else if (progress <= 90) {
                    ContextCompat.getColor(requireContext(), R.color.color_blue)
                } else {
                    ContextCompat.getColor(requireContext(), R.color.color_green)
                }

                bubbleSeekBar.setSecondTrackColor(color)
                bubbleSeekBar.setThumbColor(color)
                bubbleSeekBar.setBubbleColor(color)

                // When the user selects a valid entry value, enable the START button
                if (progress > 0) btn_start?.enabled = true
            }

            override fun getProgressOnActionUp(
                bubbleSeekBar: BubbleSeekBar,
                progress: Int,
                progressFloat: Float
            ) {
                // Do something when the user stops moving the SeekBar
                tv_distance?.text = "Select START for a $progress m run!"
            }

            override fun getProgressOnFinally(
                bubbleSeekBar: BubbleSeekBar,
                progress: Int,
                progressFloat: Float,
                fromUser: Boolean
            ) {
                // Do Something?
            }

        }


        if (dataLocation.RACE_STARTED){
            btn_pause?.enabled = true
            btn_start?.text = getString(R.string.btn_stop)
        } else false
    }

    private fun startRace() {

        btn_pause?.enabled = true
        dataLocation.RACE_STARTED = true
        btn_start?.text = (getString(R.string.btn_stop))
        Toast.makeText(
            requireContext(),
            getString(R.string.race_started),
            Toast.LENGTH_LONG
        )
            .show()

        dataLocation.mRaceDistance = seek_distance!!.progressFloat
        Toast.makeText(
            requireContext(),
            "${dataLocation.mRaceDistance}m " + getString(R.string.race_started),
            Toast.LENGTH_LONG
        )
            .show()

        dataLocation

        MainActivity.navController!!.navigate(R.id.nav_race_current)
    }
}
