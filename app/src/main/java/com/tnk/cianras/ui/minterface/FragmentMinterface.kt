package com.tnk.cianras.ui.minterface

import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.tnk.cianras.R
import com.tnk.cianras.database.location.DataLocation
import com.tnk.cianras.database.intervals.DataIntervals
import kotlinx.android.synthetic.main.obj_time_main.*
import kotlinx.android.synthetic.main.obj_time_race.*

class FragmentMinterface : Fragment() {

    private val TAG: String = "Minterface :: "

    private var isWorking: Boolean = false
    internal var MillisecondTime: Long = 0
    internal var StartTime: Long = 0
    internal var TimeBuff: Long = 0
    internal var UpdateTime = 0L

    var Hours: Int = 0
    var Seconds: Int = 0
    var Minutes: Int = 0
    var MilliSeconds: Int = 0

    var currentTimer: CountDownTimer? = null

    private var raceTimeHandler: Handler? = null
    private var mainTimeHandler: Handler? = null

    var tv_hour: TextView? = null
    var tv_minute: TextView? = null
    var tv_seconds: TextView? = null
    var tv_milliseconds: TextView? = null
    var tv_currentInterval: TextView? = null
    private var tv_location: TextView? = null
    private var tv_distance: TextView? = null

    private var dataIntervals: DataIntervals = DataIntervals
    private var dataLocation: DataLocation =
        DataLocation

    var chronometer1: Chronometer? = null
    var btn_start: Button? = null
    var btn_pause: Button? = null
    var seeker_distance: SeekBar? = null
    var seeker_progress: SeekBar? = null

    lateinit var minterfaceViewModel: MinterfaceViewModel

    companion object {

        fun newInstance(): FragmentMinterface {
            return FragmentMinterface()
        }

        // Race Variables
        //var mRaceDistance: Float = 0.0f
        var mRaceTime: Long = 0
        var mUptime: Long = 0

        internal var Race_MillisecondTime: Long = 0
        internal var Race_StartTime: Long = 0
        internal var Race_TimeBuff: Long = 0
        internal var Race_UpdateTime = 0L
        internal var Race_Hours: Int = 0
        internal var Race_Seconds: Int = 0
        internal var Race_Minutes: Int = 0
        internal var Race_MilliSeconds: Int = 0
        internal var Race_PauseTime: Long = 0
        var tv_race_hour: TextView? = null
        var tv_race_minute: TextView? = null
        var tv_race_seconds: TextView? = null
        var tv_race_milliseconds: TextView? = null
    }

    init {
        raceTimeHandler = Handler()
        mainTimeHandler = Handler()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        minterfaceViewModel = ViewModelProviders.of(this).get(MinterfaceViewModel::class.java)
        val root = inflater.inflate(R.layout.frag_minterface, container, false)
        //
        initUI(root)
        //
        return root
    }
    /**
     * @LABEL LIFECYCLE
     */
    override fun onDetach() {
        super.onDetach()
        Log.v(TAG, "onDetach()")
        raceTimeHandler?.removeCallbacks(runnableTimerMain)
        raceTimeHandler?.removeCallbacks(runnableTimerRace)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.v(TAG, "onDestroyView()")
        raceTimeHandler?.removeCallbacks(runnableTimerMain)
        raceTimeHandler?.removeCallbacks(runnableTimerRace)
    }

    /**
     * Initialize the User Interface for the Minimal Interface
     */
    private fun initUI(root: View) {
        StartTime = SystemClock.uptimeMillis()
        // @LABEL CURRENT TIME TextViews
        //tv_hour = root.findViewById(R.id.tv_ct_hour)
        //tv_minute = root.findViewById(R.id.tv_ct_minute)
        //tv_seconds = root.findViewById(R.id.tv_ct_second)
        //tv_milliseconds = root.findViewById(R.id.tv_ct_milsecond)
        // @LABEL MINIMUM INTERFACE TextViews
        tv_currentInterval = root.findViewById(R.id.tv_min_interval)
        tv_location = root.findViewById(R.id.tv_min_location)
        // @LABEL MINIMUM INTERFACE WIDGETS
        chronometer1 = root.findViewById(R.id.chrono_race)
        chronometer1?.setOnChronometerTickListener { object : Chronometer.OnChronometerTickListener {
            override fun onChronometerTick(chronometer: Chronometer?) {
                Log.v(TAG, "TICK!!")
            }
        } }
        btn_start = root.findViewById(R.id.button_start)
        btn_start?.text = getString(R.string.start)
        btn_start?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (!isWorking) {
                    //StartTime = SystemClock.uptimeMillis()
                    Race_StartTime = SystemClock.uptimeMillis()
                    chronometer1?.base = SystemClock.elapsedRealtime()
                    chronometer1?.start()
                    isWorking = true
                    btn_start?.text = getString(R.string.stop)
                    Toast.makeText(requireContext(), getString(R.string.working), Toast.LENGTH_LONG)
                        .show()
                    //system timer
                    raceTimeHandler?.postDelayed(runnableTimerRace, 0)
                    currentTimer?.start()
                    //dataLocation.RACE_PAUSED = false
                } else {
                    chronometer1?.stop()
                    //dataLocation.RACE_COMPLETE = false  // @LABEL Improve this!!
                    isWorking = false
                    btn_start?.text = getString(R.string.start)
                    Toast.makeText(requireContext(), getString(R.string.stopped), Toast.LENGTH_LONG)
                        .show()
                    raceTimeHandler?.removeCallbacks(runnableTimerRace)
                    currentTimer?.cancel()
                }
            }
        })

        btn_pause = root.findViewById(R.id.button_pause)
        btn_pause?.text = "PAUSE"
        btn_pause?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (!dataLocation.RACE_PAUSED) {
                    //dataLocation.RACE_PAUSED = true
                    Race_PauseTime = Race_UpdateTime
                    btn_pause?.text = "PAUSED"
                } else {
                    //dataLocation.RACE_PAUSED = false
                    Race_StartTime = SystemClock.uptimeMillis() - Race_PauseTime
                    btn_pause?.text = "PAUSE"
                }
            }
        })

        raceTimeHandler = Handler()
        // @LABEL Main Timer
        raceTimeHandler?.postDelayed(runnableTimerMain, 0)

        // @LABEL SeekBar Init
        seeker_distance = root.findViewById(R.id.seek_distance)
        seeker_distance!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                //TODO("Not yet implemented")
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                //TODO("Not yet implemented")
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                //dataLocation.mRaceDistance = seeker_distance!!.progress * dataLocation.mUIDivisor
                Toast.makeText(
                    requireContext(), "Distance is: " + dataLocation.mRaceDistance,
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

        seeker_progress = root.findViewById(R.id.seek_progress)
        seeker_progress?.max = (dataLocation.mRaceDistance / dataLocation.mUIDivisor).toInt()

    }

    /**
     *
     */
    fun updateCountdownUI(mills: Long) {
        Seconds = (mills / 1000).toInt()
        Minutes = Seconds / 60
        Hours = Minutes / 60
        Seconds = Seconds % 60

        MilliSeconds = (UpdateTime % 1000).toInt()

        if (Minutes.toString().length < 2) {
            tv_minute?.text = "0" + Minutes.toString()
        } else tv_minute?.text = Minutes.toString()
        if (Seconds.toString().length < 2) {
            tv_seconds?.text = "0" + Seconds.toString()
        } else tv_seconds?.text = Seconds.toString()
        tv_milliseconds?.text = MilliSeconds.toString()
        tv_hour?.text = Hours.toString()
    }

    /**
     *  This is a pause-able Timer
     */
    var runnableTimerRace: Runnable = object : Runnable {

        override fun run() {
            Race_MillisecondTime = SystemClock.uptimeMillis() - Race_StartTime
            Race_UpdateTime = Race_TimeBuff + Race_MillisecondTime

            if(!dataLocation.RACE_PAUSED) {
                Race_Seconds = (Race_UpdateTime / 1000).toInt()
                Race_Minutes = Race_Seconds / 60
                Race_Hours = Race_Minutes / 60
                Race_Seconds = Race_Seconds % 60
                Race_MilliSeconds = (UpdateTime % 1000).toInt()
            }
            // @LABEL Race Milliseconds
            tv_rt_millisecond.text = Race_MilliSeconds.toString()
            // @LABEL Race Seconds
            if (Race_Seconds.toString().length < 2) tv_rt_second?.text =
                "0" + Race_Seconds.toString()
            else tv_rt_second?.text = Race_Seconds.toString()
            // @LABEL Race Minutes
            if (Race_Minutes.toString().length < 2) tv_rt_minute?.text =
                "0" + Race_Minutes.toString()
            else tv_rt_minute?.text = Race_Minutes.toString()
            // @LABEL Race Hour
            tv_rt_hour.text = Race_Hours.toString()

            raceTimeHandler?.postDelayed(this, 0)
        }

    }

    /**
     *
     */
    var runnableTimerMain: Runnable = object : Runnable {
        override fun run() {


                MillisecondTime = SystemClock.uptimeMillis() - StartTime

                UpdateTime = TimeBuff + MillisecondTime
                Seconds = (UpdateTime / 1000).toInt()
                Minutes = Seconds / 60
                Hours = Minutes / 60
                Seconds = Seconds % 60

                MilliSeconds = (UpdateTime % 1000).toInt()

            // @LABEL Main Milliseconds
            tv_mt_milsecond.text = MilliSeconds.toString()
            // @LABEL Main Seconds
            if (Seconds.toString().length < 2) tv_mt_second?.text =
                "0" + Seconds.toString()
            else tv_mt_second?.text = Seconds.toString()
            // @LABEL Main Minutes
            if (Minutes.toString().length < 2) tv_mt_minute?.text =
                "0" + Minutes.toString()
            else tv_mt_minute?.text = Minutes.toString()
            // @LABEL Main Hour
            tv_mt_hour.text = Hours.toString()

            //mRaceTime = chronometer1!!.base

            val progress = (dataLocation.mTotalDistance / dataLocation.mUIDivisor)
            if(progress<=100){
                seeker_progress!!.progress = progress.toInt()
            } else {
                //dataLocation.RACE_COMPLETE = true
            }
            raceTimeHandler?.postDelayed(this, 0)
        }

    }

}