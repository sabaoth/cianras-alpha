package com.tnk.cianras.ui.races

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProviders
import com.beust.klaxon.*
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import java.net.URL
import java.util.HashMap
import com.tnk.cianras.R
import com.tnk.cianras.database.location.RACE_TYPES
import com.tnk.cianras.database.races.*


class RacesFragment : Fragment() {

    // FIXME Convert this to handle viewing entries in the Race Database

    private val TAG: String = "RacesActivity"

    private lateinit var mLocationCallback: LocationCallback
    private lateinit var raceViewModelRaces: ViewModelRaces

    //
    lateinit var mContext: AppCompatActivity
    private lateinit var raceListView: ExpandableListView
    private lateinit var raceListAdapter: ExpandableRaceListAdapter
    private lateinit var exListDetail: HashMap<RACE_TYPES, List<RaceItem>>
    private lateinit var exListTitle: ArrayList<RACE_TYPES>

    init {
        exListDetail = ExpandableListDataPump.getRaceListData()
        exListTitle = ArrayList<RACE_TYPES>(exListDetail.keys)
        // TODO Load values from a file?!
        /**
        // 1
        val recipeList = Recipe.getRecipesFromFile("recipes.json", this)
        // 2
        val listItems = arrayOfNulls<String>(recipeList.size)
        // 3
        for (i in 0 until recipeList.size) {
        val recipe = recipeList[i]
        listItems[i] = recipe.title
        }
        // 4
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems)
        listView.adapter = adapter
         */
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        raceViewModelRaces =
            ViewModelProviders.of(this).get(ViewModelRaces::class.java)
        val root = inflater.inflate(R.layout.frag_races, container, false)
        //
        initUI(root)
        //
        return root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context as AppCompatActivity
    }

    /**
     * Initialize the User Interface
     */
    private fun initUI(root: View) {
        //mLatitudeText = findViewById(R.id.tv_lat)
        //mLongitudeText = findViewById(R.id.tv_long)
        //mAltitudeText = findViewById(R.id.tv_alt)
        //mLocationCountText = findViewById(R.id.tv_locationCount)
        // @LABEL ListView

        //val dbInterface = RacesDbInterface(requireContext())
        //val elvCursor = dbInterface.getELVData()
        //val elvParents = elvCursor.

        val dbHandler: RaceDbHandler = RaceDbHandler(requireContext())

        val elvTitles = dbHandler.elvTitles()
        val elvDetails = dbHandler.elvDetails()

        // @LABEL
        raceListAdapter =
            ExpandableRaceListAdapter(
                requireContext(),
                elvTitles,
                elvDetails
            )

        // <ELV START>
        raceListView = root.findViewById(R.id.elv_races) as ExpandableListView
        raceListView.setAdapter(raceListAdapter)

        // @LABEL
        raceListView.setOnGroupClickListener { expandableListView, view, i, l ->
            if (expandableListView.isGroupExpanded(i)) {
                //var imgView = view.findViewById<ImageView>(R.id.img_type)
                //imgView.setImageDrawable(getDrawable(R.drawable.common_full_open_on_phone))
            } else {
                // TODO Do something when the group is expanded.
                //var imgView = view.findViewById<ImageView>(R.id.img_type)
                //imgView.setImageDrawable(getDrawable(R.drawable.common_google_signin_btn_icon_dark))
            }
            return@setOnGroupClickListener false
        }

        // @LABEL
        raceListView.setOnGroupExpandListener { groupPosition: Int ->
            Toast.makeText(
                requireContext(),
                elvTitles.get(groupPosition) + " List Expanded.",
                Toast.LENGTH_SHORT
            ).show()
        }

        // @LABEL
        raceListView.setOnGroupCollapseListener { groupPosition: Int ->
            Toast.makeText(
                requireContext(),
                elvTitles.get(groupPosition) + " List Collapsed.",
                Toast.LENGTH_SHORT
            ).show()
        }

        // @LABEL
        raceListView.setOnChildClickListener { expandableListView,
                                               view,
                                               groupPosition,
                                               childPosition,
                                               long ->

            Toast.makeText(
                requireContext(),
                elvTitles.get(groupPosition)
                        + " -> "
                        + exListDetail.get(
                    exListTitle.get(groupPosition)
                )!!.get(
                    childPosition
                ), Toast.LENGTH_SHORT
            ).show()

            return@setOnChildClickListener false
        }
        // <ELV END>
    }
}
