package com.tnk.cianras.ui.location

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.model.LatLngBounds
import com.tnk.cianras.R
import com.tnk.cianras.database.location.DataLocation

class FragmentRaceLocation : Fragment() {

    private val TAG = "FragRaceLocation :: "

    companion object {
        fun newInstance(): FragmentRaceLocation {
            return FragmentRaceLocation()
        }

        var tv_lat: TextView? = null
        var tv_long: TextView? = null
        var tv_distance: TextView? = null
    }

    private var dataLocation: DataLocation =
        DataLocation
    lateinit var mContext: AppCompatActivity
    private var mLatLng = LatLngBounds.Builder()

    init {

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context as AppCompatActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //minterfaceViewModel = ViewModelProviders.of(this).get(MinterfaceViewModel::class.java)
        //val root = inflater.inflate(R.layout.fragm, container, false)
        //
        val root = inflater.inflate(R.layout.con_race_location, container, false)
        tv_lat = root.findViewById(R.id.tv_race_lat)
        tv_long = root.findViewById(R.id.tv_race_long)
        tv_distance = root.findViewById(R.id.tv_race_distance)

        return root
    }

    /**
     * Very messy means of updating the UI
     * @author Sabaoth87
     *
     *
     */
    fun updateUI() {
        // FragmentLocation UI
        tv_lat?.text = DataLocation.mCurrentLocation?.latitude.toString()
        tv_long?.text = DataLocation.mCurrentLocation?.longitude.toString()
        tv_distance?.text = DataLocation.mTotalDistance.toString()
        // FragmentMinterface UI

    }
}