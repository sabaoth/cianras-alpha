package com.tnk.cianras.ui.progress

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.tnk.cianras.R
import com.xw.repo.BubbleSeekBar
import kotlinx.android.synthetic.main.con_race_progress.*

class FragmentRaceProgress : Fragment() {

    private val TAG: String = "FragRaceProgress"

    companion object {
        var bubbleProgress: BubbleSeekBar? = null
    }

    init {
        Log.v(TAG, "init start")


        Log.v(TAG, "init complete")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.con_race_progress, container, false)

        initUI(root)

        return root
    }

    private fun initUI(root: View) {
        bubbleProgress = root.findViewById(R.id.bubble_progress)
        bubbleProgress?.configBuilder
            ?.min(0.0f)
            ?.max(100f)
            ?.progress(0f)
            ?.sectionCount(5)
            ?.trackColor(ContextCompat.getColor(requireContext(), R.color.color_gray))
            ?.secondTrackColor(ContextCompat.getColor(requireContext(), R.color.color_blue))
            ?.thumbColor(ContextCompat.getColor(requireContext(), R.color.color_blue))
            ?.showSectionText()
            ?.sectionTextColor(ContextCompat.getColor(requireContext(), R.color.colorPrimary))
            ?.sectionTextSize(18)
            ?.showThumbText()
            ?.thumbTextColor(ContextCompat.getColor(requireContext(), R.color.color_red))
            ?.thumbTextSize(18)
            ?.bubbleColor(ContextCompat.getColor(requireContext(), R.color.color_green))
            ?.bubbleTextSize(18)
            ?.showSectionMark()
            ?.seekBySection()
            ?.autoAdjustSectionMark()
            ?.sectionTextPosition(BubbleSeekBar.TextPosition.BELOW_SECTION_MARK)
            ?.build()
    }

}