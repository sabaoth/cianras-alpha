package com.tnk.cianras.ui.minterface

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MinterfaceViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Minimum Interface Fragment"
    }
    val text: LiveData<String> = _text
}