package com.tnk.cianras.ui.intervals

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.tnk.cianras.R
import kotlinx.android.synthetic.main.con_race_interval.*

class FragmentRaceInterval : Fragment() {
    private val TAG: String = "FragRaceInterval :: "

    companion object;

    init {
        Log.v(TAG, "init start")


        Log.v(TAG, "init complete")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.con_race_interval, container, false)

        initUI(root)

        return root
    }

    private fun initUI(root: View) {

    }
}