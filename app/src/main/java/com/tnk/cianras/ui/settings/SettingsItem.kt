package com.tnk.cianras.ui.settings

import com.tnk.cianras.database.location.SETTINGS_TYPES

class SettingsItem (setName: String,
                    setType: SETTINGS_TYPES,
                    setValue: String,
                    setDesc: String)
{
    var name: String = ""
    var settingsValue: String = ""
    var valueBOOL: Boolean = false
    var valueLong: Long = -1
    var valueFloat: Float = -1f
    var description: String = ""

    init{
        this.name = setName
        this.settingsValue = setValue

        when (setType) {
            SETTINGS_TYPES.BOOL -> itsBoolean()
            SETTINGS_TYPES.LONG -> valueLong = setValue.toLong()
            SETTINGS_TYPES.FLOAT -> valueFloat = setValue.toFloat()
        }

        this.description = setDesc
    }

    private fun itsBoolean() {
        when (settingsValue) {
            "TRUE" -> valueBOOL = true
            "FALSE" -> valueBOOL = false
        }
    }
}