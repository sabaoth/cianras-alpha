package com.tnk.cianras.ui.location

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.tnk.cianras.R
import com.tnk.cianras.database.location.DataLocation
import com.tnk.cianras.database.intervals.DataIntervals
import kotlinx.android.synthetic.main.obj_current_location.*

class FragmentLocation : Fragment(), OnMapReadyCallback {

    private val TAG: String = "FragmentLocation :: "

    companion object {
        private lateinit var mMap: GoogleMap

        fun newInstance(): FragmentLocation {
            return FragmentLocation()
        }
    }

    private var dataIntervals: DataIntervals = DataIntervals
    private var dataLocation: DataLocation =
        DataLocation

    //private lateinit var mMap: GoogleMap
    private lateinit var lvLocations: ListView
    lateinit var mContext: AppCompatActivity
    private val mLatLngBounds = LatLngBounds.Builder()
    // Provides the entry point to the Fused Location Provider API
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private var mLatLng = LatLngBounds.Builder()

    init {


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context as AppCompatActivity
    }

    override fun onDetach() {
        super.onDetach()
        DataLocation.MAPPING = false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // @LABEL GOOGLE MAP
        val fm = mContext.supportFragmentManager
        val fragmentTransaction: FragmentTransaction
        val fragment = SupportMapFragment()
        fragmentTransaction = fm.beginTransaction()
        fragmentTransaction.replace(R.id.map, fragment)
            .addToBackStack(null)
        fragment.getMapAsync(this)
        fragmentTransaction.commit()
        //minterfaceViewModel = ViewModelProviders.of(this).get(MinterfaceViewModel::class.java)
        val root = inflater.inflate(R.layout.frag_location, container, false)
        //
        initUI(root)
        //mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        //createLocationCallback()
        return root
    }

    /* Request updates at startup */
    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        Log.v(TAG, "onMapReady()")

        mMap = googleMap
        DataLocation.MAPPING = true
        // declare bounds object to fit whole route in screen

        // Add a marker in Sydney and move the camera
        val point1 = LatLng(44.346404, -77.335264)
        val point2 = LatLng(44.304893, -77.339508)

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point1, 14.0f))

        /**
        mMap.addMarker(MarkerOptions().position(point1).title("Marker 1"))
        mMap.addMarker(MarkerOptions().position(point2).title("Marker 2"))

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point1, 11f))
        mMap.uiSettings.setAllGesturesEnabled(true)
        mMap.addMarker(MarkerOptions().position((point2)))

        val options = PolylineOptions()
        options.color(Color.RED)
        options.width(5f)

        val url = getURL(point1, point2)

        async {
            // here you put the potentially blocking code
            Log.v(TAG, "STARTING << async >>")

            val result = URL(url).readText()

            Log.v(TAG, "<< READING DIRECTIONS RESULT >> " + result.length)

            uiThread {
                // this will execute in the main thread, after the async call is done
                // When API call is done, create parser and convert into JsonObject
                val parser: Parser = Parser()
                val stringBuilder: StringBuilder = StringBuilder(result)
                val json: JsonObject = parser.parse(stringBuilder) as JsonObject
                // get to the correct element in JsonObject
                val routes = json.array<JsonObject>("routes")
                Log.v(TAG, "checking routes!" + result.toString())
                Log.v(TAG, "routes !empty!")
                val points = routes!!["legs"]["steps"][0] as JsonArray<JsonObject>
                // For every element in the JsonArray, decode the polyline string and pass all points to a List
                val polypts =
                    points.flatMap { decodePoly(it.obj("polyline")?.string("points")!!) }
                // Add  points to polyline and bounds
                options.add(point1)
                mLatLngBounds.include(point1)
                for (point in polypts) {
                    options.add(point)
                    mLatLngBounds.include(point)
                }
                options.add(point2)
                mLatLngBounds.include(point2)
                // build bounds
                val bounds = mLatLngBounds.build()
                // add polyline to the map
                mMap.addPolyline(options)
                // show map with route centered
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))

            }
        */

    }
    /**
     * Very messy means of updating the UI
     * @author Sabaoth87
     *
     *
     */
    fun updateUI() {
        // FragmentLocation UI
        tv_cl_lat.text = DataLocation.mCurrentLocation!!.latitude.toString()
        tv_cl_long.text = DataLocation.mCurrentLocation!!.longitude.toString()
        tv_cl_distance.text = DataLocation.mTotalDistance.toString()
        // FragmentMinterface UI

    }
    /**
     * www.medium.com tutorial @irenenaya
     */
    private fun getURL(from: LatLng, to: LatLng): String {
        val origin = "origin=" + from.latitude + "," + from.longitude // for LatLng
        val dest = "destination=" + to.latitude + "," + to.longitude    // for LatLng
        //val origin ="origin=" + nameOrigin
        //val dest ="destination=" + nameDestination
        val key = "key=" + getString(R.string.google_maps_key)
        val params = "$origin&$dest&$key"

        return "https://maps.googleapis.com/maps/api/directions/json?$params"
    }

    /**
     * Method to decode polyline points
     * Courtesy : https://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     */
    private fun decodePoly(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(
                lat.toDouble() / 1E5,
                lng.toDouble() / 1E5
            )
            poly.add(p)
        }

        return poly
    }

    /**
     * @author Sabaoth87
     *
     * Initialize the User Interface elements, and their supporting parts
     *
     */
    private fun initUI(root: View) {
        // Deprecated the ListView during debugging
        // @LABEL ListView Locations
        // @LABEL ListView
        //dataLocation.laLocations =
        //    ListAdapter_Location(requireContext(), dataLocation.mLocationsList)
        //lvLocations = root.findViewById<ListView>(R.id.lv_locations)
        //lvLocations.adapter = dataLocation.laLocations
    }

    /**
     *
     */
    fun updateMap(locale: LatLng) {
        if(DataLocation.MAPPING) {
            Log.v(TAG, "Updating Map")
            mMap.addMarker(
                MarkerOptions().position(locale).title(dataLocation.mLocationsList.size.toString())
            )
            mMap.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    locale,
                    dataLocation.mZoomLevel
                )
            )
            mLatLng.include(locale)
            //tv_min_distance.text = ataLocation
            //val bounds = mLatLng.build()
            //mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 1000))
            mMap.maxZoomLevel
        }
    }

}