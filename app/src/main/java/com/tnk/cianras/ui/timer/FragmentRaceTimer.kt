package com.tnk.cianras.ui.timer

import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.tnk.cianras.R
import com.tnk.cianras.database.location.DataLocation
import com.tnk.cianras.ui.progress.FragmentRaceProgress
import kotlinx.android.synthetic.main.obj_time_race.*

class FragmentRaceTimer : Fragment() {

    private val TAG: String = "FragmentRaceTimer :: "
    //private var dataLocations: DataLocation = DataLocation
    private var uiProgress: FragmentRaceProgress? = null

    companion object {
        fun newInstance(): FragmentRaceTimer {
            return FragmentRaceTimer()
        }

        // Race Variables
        //var mRaceDistance: Float = 0.0f
        var mRaceTime: Long = 0
        var mUptime: Long = 0

        private var isTiming: Boolean = false

        internal var Race_MillisecondTime: Long = 0
        internal var Race_StartTime: Long = 0
        internal var Race_TimeBuff: Long = 0
        internal var Race_UpdateTime = 0L
        internal var Race_Hours: Int = 0
        internal var Race_Seconds: Int = 0
        internal var Race_Minutes: Int = 0
        internal var Race_MilliSeconds: Int = 0
        internal var Race_PauseTime: Long = 0
        var tv_race_hour: TextView? = null
        var tv_race_minute: TextView? = null
        var tv_race_seconds: TextView? = null
        var tv_race_milliseconds: TextView? = null
        var raceTimeHandler: Handler? = null
    }

    init {
        Log.v(TAG, "init start")
        raceTimeHandler = Handler()
        uiProgress = FragmentRaceProgress()
        Log.v(TAG, "init complete")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.con_race_timer, container, false)

        initUI(root)

        return root
    }

    private fun initUI(root: View) {
        raceTimeHandler = Handler()
        // @LABEL Race Timer Automatically
        raceTimeHandler?.postDelayed(runnableTimerRace, 0)

        // @LABEL Start a Race
        if(!isTiming){
            Race_StartTime = SystemClock.uptimeMillis()
            raceTimeHandler?.postDelayed(runnableTimerRace, 0)
            isTiming = true
            //DataLocation.RACE_PAUSED = false
        }
    }

    // @LABEL LIFECYCLE
    override fun onDetach() {
        super.onDetach()
        Log.v(TAG, "onDetach()")
        raceTimeHandler?.removeCallbacks(runnableTimerRace)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.v(TAG, "onDestroyView()")
        raceTimeHandler?.removeCallbacks(runnableTimerRace)
    }
    // @LABEL LIFECYCLE


    /**
     *  This is a pause-able Timer
     */
    private var runnableTimerRace: Runnable = object : Runnable {

        override fun run() {
            Race_MillisecondTime = SystemClock.uptimeMillis() - Race_StartTime
            Race_UpdateTime = Race_TimeBuff + Race_MillisecondTime

            if(DataLocation.RACE_STARTED) {
                if (!DataLocation.RACE_COMPLETED) {
                    Race_Seconds = (Race_UpdateTime / 1000).toInt()
                    Race_Minutes = Race_Seconds / 60
                    Race_Hours = Race_Minutes / 60
                    Race_Seconds = Race_Seconds % 60
                    Race_MilliSeconds = (Race_UpdateTime % 1000).toInt()
                }
            }
            // @LABEL Race Milliseconds
            tv_rt_millisecond.text = Race_MilliSeconds.toString()
            // @LABEL Race Seconds
            if (Race_Seconds.toString().length < 2) tv_rt_second?.text =
                "0" + Race_Seconds.toString()
            else tv_rt_second?.text = Race_Seconds.toString()
            // @LABEL Race Minutes
            if (Race_Minutes.toString().length < 2) tv_rt_minute?.text =
                "0" + Race_Minutes.toString()
            else tv_rt_minute?.text = Race_Minutes.toString()
            // @LABEL Race Hour
            tv_rt_hour.text = Race_Hours.toString()


            if(DataLocation.mRaceProgress>=1){
                //RaceActivity.!!.progress = dataLocations.mRaceProgress.toInt()
                if(DataLocation.mRaceProgress>100) {
                    FragmentRaceProgress.bubbleProgress?.configBuilder
                        ?.progress(100.0f)
                        ?.build()
                    DataLocation.RACE_COMPLETE = true
                    DataLocation.RACE_COMPLETED = true
                } else {
                    FragmentRaceProgress.bubbleProgress?.configBuilder
                        ?.progress(DataLocation.mRaceProgress)
                        ?.build()
                }
                //bubble_progress.progress = dataLocations.mRaceProgress.toInt()
            }

            raceTimeHandler?.postDelayed(this, 0)
        }

    }
}