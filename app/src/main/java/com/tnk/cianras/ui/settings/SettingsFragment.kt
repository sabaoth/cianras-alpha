package com.tnk.cianras.ui.settings

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.tnk.cianras.R

class SettingsFragment : PreferenceFragmentCompat(){

    private val TAG: String = "Settings ::"     // for debugging callouts

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }
}