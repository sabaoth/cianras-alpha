package com.tnk.cianras.database.races

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import com.tnk.cianras.R
import com.tnk.cianras.database.location.RACE_TYPES
import kotlinx.android.synthetic.main.item_race_child.view.*
import kotlinx.android.synthetic.main.item_race_parent.view.*

class ExpandableRaceListAdapter(var _context: Context,
                                var expandableListTitle: List<String>,
                                var expandableListDetail: HashMap<RACE_TYPES, List<RaceItem>>
) : BaseExpandableListAdapter()
{
    init {
        this._context = _context
        this.expandableListDetail = expandableListDetail
        this.expandableListTitle = expandableListTitle
    }

    override fun getChild(listPosition: Int, expandedListPosition: Int): RaceItem {
        // FIXME Consider converting this to function
        val parentType = this.expandableListTitle.get(listPosition)
        var key: RACE_TYPES = RACE_TYPES.NONE

        if (parentType == "FREERUN") {
            key = RACE_TYPES.FREERUN
        }
        if (parentType == "CROSS") {
            key = RACE_TYPES.CROSS
        }
        if (parentType == "RUN") {
            key = RACE_TYPES.RUN
        }
        if (parentType == "RACE") {
            key = RACE_TYPES.RACE
        }
        if (parentType == "SPRINT") {
            key = RACE_TYPES.SPRINT
        }
        if (parentType == "WALK") {
            key = RACE_TYPES.WALK
        }
        //val child = expandableListDetail.get(key)!!.get(expandedListPosition)
        //return this.expandableListDetail.get(this.expandableListTitle!!.get(listPosition))!!.get(expandedListPosition)
        return expandableListDetail.get(key)!!.get(expandedListPosition)
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int,
                              isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView

        val raceChild = getChild(groupPosition, childPosition)
        val childText = raceChild.raceTime

        if (convertView == null) {
            val infalInflater = this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = infalInflater.inflate(R.layout.item_race_child, null)
        }

        // TODO POPULATE THE childView TextViews
        convertView?.tv_type?.text = raceChild.raceType.name
        convertView?.tv_distance?.text = raceChild.raceDistance
        convertView?.tv_time?.text = raceChild.raceTime
        convertView?.tv_location?.text = raceChild.raceLocation

        /*val txtListChild = convertView!!
                .findViewById(R.id.lblListItem) as TextView
        txtListChild.text = childText
        */
        return convertView!!
    }

    override fun getChildrenCount(listPosition: Int): Int {
        // FIXME Consider converting this to function
        val parentType = this.expandableListTitle.get(listPosition)
        var key: RACE_TYPES = RACE_TYPES.NONE
        if (parentType == "FREERUN") {
            key = RACE_TYPES.FREERUN
        }
        if (parentType == "CROSS") {
            key = RACE_TYPES.CROSS
        }
        if (parentType == "RUN") {
            key = RACE_TYPES.RUN
        }
        if (parentType == "RACE") {
            key = RACE_TYPES.RACE
        }
        if (parentType == "SPRINT") {
            key = RACE_TYPES.SPRINT
        }
        if (parentType == "WALK") {
            key = RACE_TYPES.WALK
        }
        return expandableListDetail.get(key)!!.size
    }

    override fun getGroup(listPosition: Int): Any {
        return this.expandableListTitle.get(listPosition)
    }

    override fun getGroupCount(): Int {
        return this.expandableListTitle.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                              convertView: View?, parent: ViewGroup
    ): View {
        var convertView = convertView
        val headerTitle = getGroup(groupPosition) as String
        if (convertView == null) {
            val infalInflater = this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = infalInflater.inflate(R.layout.item_race_parent, null)
        }

        if (groupPosition % 2 == 1) {
            convertView?.setBackgroundResource(R.color.colorPrimary)
        } else {
            convertView?.setBackgroundResource(R.color.colorAccent)
        }

        // TODO Populate all the TextViews of the ListView!
        convertView!!.tv_type.text = headerTitle

        return convertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

}