package com.tnk.cianras.database.races

import android.location.Location
import java.util.*

class RacePath(
    id: Int,
    name: String
) {
    private lateinit var currentPaths: HashMap<SEGMENTS, List<Location>>
    private lateinit var enumPaths: EnumMap<SEGMENTS, List<Location>>
    enum class SEGMENTS {SPRINT, RUN, JOG, WALK, REST}

    init {
        currentPaths = HashMap<SEGMENTS, List<Location>>()
    }

    fun addLocation (segment: SEGMENTS,
                     location: Location
                      ){

        currentPaths[segment]

    }

}