package com.tnk.cianras.database.events

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.util.Log
import android.widget.TextView
import com.tnk.cianras.Overseer
import com.tnk.cianras.database.location.DataLocation
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread

class DistanceTrigger(val triggerDistance: Double) {

    private val TAG: String = "DistanceTrigger :: "
    //
    private var dLocation: DataLocation =
        DataLocation
    var callerTV: TextView? = null
    var callerContext: Context? = null
    var captionText: String = ""
    var distance: Double = 0.0
    var duration: Int = 0
    var soundInt: Int = 0
    var isTriggered: Boolean = false
    var soundUri: String = ""
    var mediaPlayer: MediaPlayer? = null
    //
    companion object;
    //
    init {
        distance = triggerDistance
    }
    // Distance Triggered Raw Sourced Constructor
    // FIXME DEPRECATION
    // @LABEL I believe this deprecates the entire class EventSoundURI
    // Play to remove
    constructor(triggerDistance: Double,
                context: Context,
                sound: Int) : this(triggerDistance) {
        Log.v(TAG, "constructor")
        callerContext= context
        soundInt = sound

        loadMediaPlayer()
    }
    // Distance Triggered URI Sourced Constructor
    constructor(triggerDistance: Double,
                context: Context,
                sound: String): this(triggerDistance) {
        callerContext= context
        soundUri = sound
        loadSoundUriEvent()
    }
    // Distance Triggered Caption Constructor
    constructor(triggerDistance: Double,
                tv: TextView,
                caption: String)  :this (triggerDistance) {
        callerTV = tv
        captionText = caption
        loadCaptionEvent()
    }

    //
    init {
        Log.v(TAG, "init")
    }
    //

    private fun loadMediaPlayer() {
        // Init the MediaPlayer
        mediaPlayer = MediaPlayer.create(callerContext, soundInt)
        mediaPlayer?.setOnPreparedListener {
            Log.v(TAG, "Event at $triggerDistance m loaded!")
            duration = mediaPlayer!!.duration
            Log.v(TAG, "Duration is ${mediaPlayer!!.duration}")
        }
        // Start the async call!
        async {
            Log.v(TAG, "async init")
            while(dLocation.mTotalDistance <= triggerDistance)
            {
                isTriggered = false
            }
            mediaPlayer?.start()
            isTriggered = true
            uiThread {
                Log.v(TAG, "Event at $triggerDistance m played!")
                //mediaPlayer.stop()
                //mediaPlayer.seekTo(0)
                // = "mps $triggerDistance playing"

                // Release and Resolve the MediaPlayer
                mediaPlayer?.release()
                mediaPlayer = null
            }
        }
    }

    private fun loadSoundUriEvent() {
        // Start the async call!
        async {
            Log.v(TAG, "async init")
            while(dLocation.mTotalDistance <= triggerDistance)
            {
                isTriggered = false
            }
            // Init the MediaPlayer
            mediaPlayer = MediaPlayer().apply {
                setAudioStreamType(AudioManager.STREAM_MUSIC)
                setDataSource(soundUri)
                prepare()
                setOnPreparedListener {
                    Log.v(TAG, "Event at $triggerDistance m loaded!")
                }
                start()
            }
            isTriggered = true
            uiThread {
                Log.v(TAG, "Event at $triggerDistance m played!")
                //mediaPlayer.stop()
                //mediaPlayer.seekTo(0)
                // = "mps $triggerDistance playing"

                // Release and Resolve the MediaPlayer
                mediaPlayer?.release()
                mediaPlayer = null
            }
        }
    }

    /**
     * Loads a caption thread
     * This caption is also sent to the TTS Engine
     *
     * @author sabaoth87
     */
    private fun loadCaptionEvent() {
        async {
            Log.v(TAG, "async init")
            while(dLocation.mTotalDistance < triggerDistance)
            {
                isTriggered = false
            }
            isTriggered = true

            uiThread {
                callerTV?.text = captionText
                Overseer.speakOut(captionText)
            }
        }
    }

}