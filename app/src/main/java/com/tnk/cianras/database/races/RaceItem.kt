package com.tnk.cianras.database.races

import com.tnk.cianras.database.location.RACE_TYPES

data class RaceItem(val type: RACE_TYPES )
{
    var raceId: Int = 0
    var raceType:       RACE_TYPES = RACE_TYPES.NONE
    var raceDate:       String = ""
    var raceDistance:   String = ""
    var raceTime:       String = ""
    var raceLocation:   String = ""

    constructor(type: RACE_TYPES,
                date: String,
                distance: String,
                time: String,
                location: String) : this(type) {
        //
        //this.raceId = Id
        this.raceType = type
        this.raceDate = date
        this.raceDistance = distance
        this.raceTime = time
        this.raceLocation = location
        //
    }

}

