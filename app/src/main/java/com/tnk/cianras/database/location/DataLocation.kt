package com.tnk.cianras.database.location

import android.content.Context
import android.location.Location
import android.util.Log
import android.widget.TextView
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import com.tnk.cianras.Overseer
import com.tnk.cianras.database.events.DistanceTrigger
import com.tnk.cianras.database.intervals.Interval
import kotlin.math.abs
import kotlin.random.Random

object DataLocation {

    private val TAG: String = "DataLocation :: "
    private lateinit var context: Context

    var mRaceDistance: Float = 2000.0f
    var mRaceProgress: Float = 0.0f
    var mRaceDistanceMax: Double = 2000.0

    var uiFence: String = ""

    var mDistances: MutableList<Double>
    var mTotalDistance: Double = 0.0
    var mDeltaDistance: Double = 0.0
    var mDistanceFence: Double = 1.50                  // @REMARK For Testing
    var mZoomLevel: Float = 14.0f
    var mLocationsList: MutableList<Location>
    var mCurrentLocation: Location? = null
    var mLastLocation: Location? = null
    var mUIDivisor: Double = 250.0

    var raceTriggers1 = mutableListOf<Double>(
        250.0,
        500.0,
        750.0,
        1000.0
    )

    lateinit var listIntervals: ArrayList<Interval>
    lateinit var listLocations: ArrayList<Location>
    lateinit var laLocations: ListAdapter_Location
    lateinit var mLocationCallback: LocationCallback
    var MAPPING: Boolean = true
    var RACE_STARTED: Boolean = false
    var RACE_PAUSED: Boolean = false
    var RACE_COMPLETE: Boolean = false
    var RACE_COMPLETED: Boolean = false

    init {
        Log.v(TAG, "init BEGINS")
        mDistances = mutableListOf<Double>()
        mLocationsList = mutableListOf<Location>()
        listLocations = ArrayList<Location>()
        listIntervals = ArrayList<Interval>()
        Log.v(TAG, "init ENDS")
    }


    /**
     * returns a distance, in meters, between two LatLng values
     * @param location1: LatLng
     * @param location2: LatLng
     * @return totalDistance: Double
     */
    fun distanceBetween(location1: Location,
                       location2: Location) : Double {
        val locale1 = LatLng(location1.latitude, location1.longitude)
        val locale2 = LatLng(location2.latitude, location2.longitude)

        val totalDistance: Double = SphericalUtil.computeDistanceBetween(locale1, locale2)

        Log.v(TAG, "Calculated $totalDistance in distanceBetween()")
        return totalDistance
    }
    /**
     * returns a distance, in meters, between two LatLng values
     * @param location1: LatLng
     * @param location2: LatLng
     * @return totalDistance: Float
     */
    fun distanceLatLng(location1: LatLng,
                       location2: LatLng) : Float {
        var totalDistance: Float
        val meterConversion = 1609
        val earthRadius = 3958.75

        val latDiff = Math.toRadians(location1.latitude - location2.latitude)
        val longDIff = Math.toRadians(location1.longitude - location2.longitude)
        val a = Math.sin(latDiff/2) * Math.sin(longDIff/2) +
                Math.cos(Math.toRadians(location1.latitude)) * Math.cos(Math.toRadians(location2.latitude)) *
                Math.sin(longDIff/2) * Math.sin(longDIff/2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))

        totalDistance = (earthRadius * c * meterConversion).toFloat()
        Log.v(TAG, "Calculated $totalDistance in distanceLatLng()")
        return totalDistance
    }

    /**
     * returns a distance, in meters, between two LatLng values
     * @param location1: Location
     * @param location2: Location
     * @return totalDistance: Float
     */
    fun distanceLocations(location1: Location,
                          location2: Location) : Float {
        var totalDistance: Float
        val meterConversion = 1609
        val earthRadius = 3958.75

        val latDiff = Math.toRadians(location1.latitude - location2.latitude)
        val longDIff = Math.toRadians(location1.longitude - location2.longitude)
        val a = Math.sin(latDiff/2) * Math.sin(longDIff/2) +
                Math.cos(Math.toRadians(location1.latitude)) * Math.cos(Math.toRadians(location2.latitude)) *
                Math.sin(longDIff/2) * Math.sin(longDIff/2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))

        totalDistance = (earthRadius * c * meterConversion).toFloat()
        Log.v(TAG, "Calculated $totalDistance in distanceLocation()")
        return totalDistance
    }

    /**
     *
     * @REMARK TEST Updates the array of Distances
     * @REMARK TEST Updates the totalDistance
     * @REMARK TEST Updates the deltaDistance
     *
     */
    fun updateDistances() {
        mDistances.clear()
        mTotalDistance = 0.0

        if (mLocationsList.size > 1) {
            val locats = mLocationsList.size
            Log.v(TAG, "mLocationSize $locats")
            // Update the Array of Distances
            for (index in 0 until (mLocationsList.size-1)) {
                // @LABEL Ensure to take the absolute value of this distance
                val distance = abs(
                    distanceBetween(
                        mLocationsList[index],
                        mLocationsList[index + 1]
                    )
                )
                mDistances.add(distance)
                Log.v(TAG, "Adding location $index.")
                Log.v(TAG, "Distance = $distance m")
                //mTotalDistance += distance
            }
            // Update the Total Distance
            for (index in mDistances) {
                // FIXME mTotalDistance is being wonky here...
                mTotalDistance += index.toFloat()
            }
            // Update the Delta Distance
            mDeltaDistance =
                distanceBetween(
                    mLocationsList.first(),
                    mLocationsList.last()
                )
        }
        // @LABEL Overall Progress of this Race
        //mRaceProgress = (mTotalDistance / mUIDivisor).toFloat()
        mRaceProgress = (mTotalDistance.toFloat() / mRaceDistance) * 100
        Log.v(TAG, "Progress = $mRaceProgress m")
        Log.v(TAG, "Race Distance = $mRaceDistance m")
        Log.v(TAG, "Total Distance = $mTotalDistance m")
        Log.v(TAG, "Delta Distance = $mDeltaDistance m")
    }

    /**
     *
     */
    fun addLocation(newLocation: Location) {
        mLocationsList.add(newLocation)
        updateDistances()
    }

    /**
     *
     */
    fun setContext(con: Context) {
        context = con
        laLocations =
            ListAdapter_Location(
                context,
                listLocations
            )
    }

    /**
     *
     */
    fun loadCaptionsByDistanceExample(): MutableList<DistanceTrigger> {
        var raceDistanceEvents = mutableListOf<DistanceTrigger>()
        raceDistanceEvents.clear()

        var runnerEvent = DistanceTrigger(2.0, Overseer.tvCaptions!!,
            Overseer.eventStrOpenings!![Random.nextInt(0,Overseer.eventStrOpenings!!.size)])
        raceDistanceEvents.add(runnerEvent)

        runnerEvent = DistanceTrigger(15.0, Overseer.tvCaptions!!,
            Overseer.eventStrEncouragements!![Random.nextInt(0,Overseer.eventStrEncouragements!!.size)])
        raceDistanceEvents.add(runnerEvent)

        runnerEvent = DistanceTrigger(100.0, Overseer.tvCaptions!!,
            Overseer.eventStrWeatherSunny!![Random.nextInt(0,Overseer.eventStrWeatherSunny!!.size)])
        raceDistanceEvents.add(runnerEvent)

        runnerEvent = DistanceTrigger(250.0, Overseer.tvCaptions!!,
            Overseer.eventStrEncouragements!![Random.nextInt(0,Overseer.eventStrEncouragements!!.size)])
        raceDistanceEvents.add(runnerEvent)

        for ((index,caption) in Overseer.eventStr100meters!!.withIndex()) {
            //
            runnerEvent = DistanceTrigger(
                ((index * 100.0) + 100.0),
                Overseer.tvCaptions!!,
                caption)
            raceDistanceEvents.add(runnerEvent)
        }

        return raceDistanceEvents
    }
}

enum class RACE_TYPES {
    NONE,
    FREERUN,
    WALK,
    RUN,
    SPRINT,
    RACE,
    CROSS
}

enum class SETTINGS_TYPES {
    BOOL,
    LONG,
    FLOAT,
    COMPLEX
}