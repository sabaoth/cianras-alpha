package com.tnk.cianras.database.races;

import android.provider.BaseColumns;

/**
 * Created by Tom on 2018-01-03.
 */

public final class RaceContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private RaceContract() {}

    /* Inner Class that defines the table contents */
    public static class RaceEntry implements BaseColumns {
        public static final String TABLE_NAME = "Races";
        public static final String COLUMN_NAME_TYPE = "raceType";
        public static final String COLUMN_NAME_DATE = "raceDate";
        public static final String COLUMN_NAME_DISTANCE = "raceDistance";
        public static final String COLUMN_NAME_TIME = "raceTime";
        public static final String COLUMN_NAME_LOCATION = "raceLocation";
    }
}