package com.tnk.cianras.database.events

import android.util.Log
import android.widget.TextView
import com.tnk.cianras.database.location.DataLocation
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread

class EventString(tv: TextView,
                  distance: Double,
                  caption: String) {

    private val TAG: String = "EventString"
    private var dLocation: DataLocation =
        DataLocation

    var triggerDistance: Double = 0.0
    var callerTV: TextView = tv
    var captionText: String = ""
    var isTriggered: Boolean = false

    //companion object {   }

    init {
        this.triggerDistance = distance
        this.callerTV = tv
        this.captionText = caption
    }

    fun loadEvent() {

        async {
            Log.v(TAG, "async init")
            while(dLocation.mTotalDistance < triggerDistance)
            {
                isTriggered = false
            }
            isTriggered = true

            uiThread {
                callerTV.text = captionText
            }
        }


    }

}