package com.tnk.cianras.database.races;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.tnk.cianras.R;
import com.tnk.cianras.database.races.RaceContract;

/**
 * Created by Tom on 2018-01-06.
 */

public class RaceCursorAdapter extends CursorAdapter {
    public RaceCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    // the newView method is used to inflate a new view and return it,
    // you do not bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_race, parent, false);
    }

    // the bindView method is used to bind all data to a given view
    // such as setting the text on a text view
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView tvBrand = (TextView) view.findViewById(R.id.item_tv_RaceTime);
        TextView tvName = (TextView) view.findViewById(R.id.item_tv_Location);
        TextView tvSize  = (TextView) view.findViewById(R.id.item_tv_RaceDistance);
        // Extract properties from cursor
        String brand = cursor.getString(cursor.getColumnIndexOrThrow(RaceContract.RaceEntry.COLUMN_NAME_TIME));
        String name = cursor.getString(cursor.getColumnIndexOrThrow(RaceContract.RaceEntry.COLUMN_NAME_LOCATION));
        String size = cursor.getString(cursor.getColumnIndexOrThrow(RaceContract.RaceEntry.COLUMN_NAME_DISTANCE));
        //Populate fields with extracted properties
        tvBrand.setText(brand);
        tvName.setText(name);
        tvSize.setText(size);
    }

}
