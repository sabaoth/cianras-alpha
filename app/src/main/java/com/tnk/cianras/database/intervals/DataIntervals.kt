package com.tnk.cianras.database.intervals

import android.os.CountDownTimer
import android.os.Handler
import android.os.SystemClock

object DataIntervals {

    var TIMING: Boolean = false
    var mIntervalList: MutableList<Interval>
    var mCountdownArray = java.util.ArrayList<CountDownTimer>()
    var mCountDownArrayPoint: Int = 0

    private var isWorking: Boolean = false
    internal var timeCurrentMils: Long = 0
    internal var timeStart: Long = 0
    internal var timeBuff: Long = 0
    internal var timeUpdate = 0L

    var Hours: Int = 0
    var Seconds: Int = 0
    var Minutes: Int = 0
    var MilliSeconds: Int = 0

    private var handler: Handler? = null

    enum class INTERVALTYPES {
        REST, JOG, RUN, SPRINT, SUICIDE
    }

    init {
        mIntervalList = mutableListOf<Interval>()
        timeStart = SystemClock.uptimeMillis()
        handler = Handler()
        //laLocations = ListAdapter_Location(context, listLocations)
    }

    var timerunner: Runnable = object : Runnable {

        override fun run() {
            timeCurrentMils = SystemClock.uptimeMillis() - timeStart

            timeUpdate = timeBuff + timeCurrentMils
            Seconds = (timeUpdate / 1000).toInt()
            Minutes = Seconds / 60
            Hours = Minutes / 60
            Seconds = Seconds % 60

            MilliSeconds = (timeUpdate % 1000).toInt()
            if (Minutes.toString().length < 2) {
                //tv_minute?.text = "0" + Minutes.toString()
            } else {
                //tv_minute?.text = Minutes.toString()
            }
            if (Seconds.toString().length < 2) {
                //tv_seconds?.text = "0" + Seconds.toString()
            } else {
                //tv_seconds?.text = Seconds.toString()
            }
            //tv_milliseconds?.text = MilliSeconds.toString()
            //tv_hour?.text = Hours.toString()
            handler?.postDelayed(this, 0)
        }

    }

    /**
     *  Send it a TIME value in Milliseconds and receive a String of the formatted time
     *     00m00s
     */
    fun millisToString(millis: Long): String {
        val seconds = (millis / 1000).toInt()
        val minutes = seconds / 60

        var timed = ""
        if (seconds > 60) {
            if (minutes.toString().length < 2) {
                timed = "0" + minutes.toString() + "m"
            } else {
                timed = minutes.toString() + "m"
            }
        }

        if (seconds.toString().length < 2) {
            timed = timed + "0" + seconds.toString() + "s"
        } else {
            timed = timed + seconds.toString() + "s"
        }
        return timed
    }

}

