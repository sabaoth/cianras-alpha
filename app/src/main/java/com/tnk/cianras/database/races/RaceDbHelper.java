package com.tnk.cianras.database.races;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

/**
 * Created by Tom on 2018-01-03.
 */

public class RaceDbHelper extends SQLiteOpenHelper {
    public static final String TAG = "RaceDbHelper";
    //If you change the database scheme, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MyRaces.db";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + RaceContract.RaceEntry.TABLE_NAME + " (" +
                    RaceContract.RaceEntry._ID + " INTEGER PRIMARY KEY," +
                    RaceContract.RaceEntry.COLUMN_NAME_TYPE + " TEXT," +
                    RaceContract.RaceEntry.COLUMN_NAME_DATE + " TEXT," +
                    RaceContract.RaceEntry.COLUMN_NAME_DISTANCE + " TEXT," +
                    RaceContract.RaceEntry.COLUMN_NAME_TIME + " TEXT," +
                    RaceContract.RaceEntry.COLUMN_NAME_LOCATION + " TEXT)";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + RaceContract.RaceEntry.TABLE_NAME;


    public RaceDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db){
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVerseion, int newVersion){
        // This database is only a cache for online data, so its upgrade policy is
        // to simply discard the data and start over
        db.execSQL(SQL_CREATE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
        onUpgrade(db, oldVersion, newVersion);
    }

    public long addRaceHandler(RaceItem tool) {
        Log.v(TAG, "Opening the Local Db for writing...");
        SQLiteDatabase db = this.getWritableDatabase();
        Log.v(TAG, "Db open!");
        ContentValues values = new ContentValues();

        values.put(RaceContract.RaceEntry.COLUMN_NAME_TYPE, tool.getRaceType().toString());
        values.put(RaceContract.RaceEntry.COLUMN_NAME_DATE, tool.getRaceDate());
        values.put(RaceContract.RaceEntry.COLUMN_NAME_DISTANCE, tool.getRaceDistance());
        values.put(RaceContract.RaceEntry.COLUMN_NAME_TIME, tool.getRaceTime());
        values.put(RaceContract.RaceEntry.COLUMN_NAME_LOCATION, tool.getRaceLocation());

        Log.v(TAG, "Adding race entry to Db...");
        long newRowId = db.insert(RaceContract.RaceEntry.TABLE_NAME, null, values);
        Log.v(TAG, "Entry " + newRowId + " added to Db");
        db.close();
        return(newRowId);
    }

    public Cursor findRaceByType(String toolType){
        SQLiteDatabase db = getReadableDatabase();

        //Define a projection that specifies which columns from the database
        // you wll actually use after this query.
        String[] projection = {
                RaceContract.RaceEntry._ID,
                RaceContract.RaceEntry.COLUMN_NAME_TYPE,
                RaceContract.RaceEntry.COLUMN_NAME_DATE,
                RaceContract.RaceEntry.COLUMN_NAME_DISTANCE,
                RaceContract.RaceEntry.COLUMN_NAME_TIME
        };
        // Filter results WHERE "type" = et_ToolType entry
        String selection = RaceContract.RaceEntry.COLUMN_NAME_TYPE + " = ?";
        String[] selectionArgs = { toolType };

        // How you want the results sorted int he resulting Cursor

        String sortOrder = RaceContract.RaceEntry.COLUMN_NAME_LOCATION + " DESC";

        Cursor cursor = db.query(
                RaceContract.RaceEntry.TABLE_NAME,  // The table you want to query
                projection,                         // The columns you want to return
                selection,                          // the columns you want to return
                selectionArgs,                     // the columns for the WHERE clause
                null,                      // the values for the WHERE clause
                null,                       // do not group the rows
                sortOrder
        );
        return cursor;
    }

    public Cursor findRaceByID(String toolId){
        SQLiteDatabase db = getReadableDatabase();

        //Define a projection that specifies which columns from the database
        // you wll actually use after this query.
        String[] projection = {
                RaceContract.RaceEntry._ID,
                RaceContract.RaceEntry.COLUMN_NAME_TYPE,
                RaceContract.RaceEntry.COLUMN_NAME_DATE,
                RaceContract.RaceEntry.COLUMN_NAME_TIME,
                RaceContract.RaceEntry.COLUMN_NAME_DISTANCE
        };
        // Filter results WHERE "type" = et_ToolType entry
        //String selection = RaceContract.RaceEntry._ID + " = ?";
        String selection = RaceContract.RaceEntry._ID + " = ?";
        String[] selectionArgs = { toolId };

        // How you want the results sorted int he resulting Cursor

        String sortOrder = RaceContract.RaceEntry._ID + " DESC";

        Cursor cursor = db.query(
                RaceContract.RaceEntry.TABLE_NAME,  // The table you want to query
                projection,                         // The columns you want to return
                selection,                          // the columns for the WHERE clause
                selectionArgs,                     // the values for the WHERE clause
                null,                      // do not group the rows
                null,                       // ?
                sortOrder                           // sort by?
        );
        return cursor;
    }

    public Cursor findAllRaces(){
        SQLiteDatabase db = getReadableDatabase();

        //Define a projection that specifies which columns from the database
        // you wll actually use after this query.
        String[] projection = {
                RaceContract.RaceEntry._ID,
                RaceContract.RaceEntry.COLUMN_NAME_TYPE,
                RaceContract.RaceEntry.COLUMN_NAME_DATE,
                RaceContract.RaceEntry.COLUMN_NAME_TIME,
                RaceContract.RaceEntry.COLUMN_NAME_LOCATION
        };
        // Filter results WHERE "type" = et_ToolType entry
        String selection = RaceContract.RaceEntry._ID + " = ?";
        String[] selectionArgs = {};

        // How you want the results sorted int he resulting Cursor

        String sortOrder = RaceContract.RaceEntry.COLUMN_NAME_TIME + " DESC";

        Cursor cursor = db.query(
                RaceContract.RaceEntry.TABLE_NAME,  // The table you want to query
                projection,                         // The columns you want to return
                null,                          // the columns you want to return
                null,                     // the columns for the WHERE clause
                null,                      // the values for the WHERE clause
                null,                       // do not group the rows
                sortOrder
        );
        return cursor;
    }

    public boolean deleteRaceById(String toolId){
        SQLiteDatabase db = getWritableDatabase();

        //Define 'where' part of the query.
        String selection = RaceContract.RaceEntry._ID + " LIKE ?";
        //Specify arguments in placeholder order.
        String[] selectionArgs = { toolId };
        //Issue the SQL statement.
        db.delete(RaceContract.RaceEntry.TABLE_NAME, selection, selectionArgs);
        return TRUE;
    }

    //@NOTE
    // This may be useless in the case of our interface for the time being.
    // Would have to have another set of activities that handle the per-tool
    // information display so that the user can have all the pertinent information
    // on one view, and the _ID has come from whatever launched such viewer
    // activities
    public boolean updateRaceById(RaceItem targetTool){
        SQLiteDatabase db = getWritableDatabase();

        //New value(s) for the column(s)
        ContentValues values = new ContentValues();
        values.put(RaceContract.RaceEntry.COLUMN_NAME_TYPE,
                targetTool.getRaceType().toString());
        values.put(RaceContract.RaceEntry.COLUMN_NAME_DATE,
                targetTool.getRaceDate());
        values.put(RaceContract.RaceEntry.COLUMN_NAME_DISTANCE,
                targetTool.getRaceDistance());
        values.put(RaceContract.RaceEntry.COLUMN_NAME_TIME,
                targetTool.getRaceTime());

        //Which row to update, based on the targetTool passed from caller
        String selection = RaceContract.RaceEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(targetTool.getRaceId()) };

        int count = db.update(
                RaceContract.RaceEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        if (count>0){
            return TRUE;
        } else {
            return FALSE;
        }
    }


}