package com.tnk.cianras.database.races

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.tnk.cianras.database.location.RACE_TYPES

class RaceDbHandler(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "MyRaces.db"
        const val TABLE_RACES = "Races"
        const val COLUMN_NAME_DATE = "raceDate"
        const val COLUMN_NAME_DISTANCE = "raceDistance"
        const val COLUMN_NAME_TIME = "raceTime"
        const val COLUMN_NAME_LOCATION = "raceLocation"

        private const val SQL_CREATE_ENTRIES =
            "CREATE TABLE " + RaceContract.RaceEntry.TABLE_NAME + " (" +
                    RaceContract.RaceEntry._ID + " INTEGER PRIMARY KEY," +
                    RaceContract.RaceEntry.COLUMN_NAME_TYPE + " TEXT," +
                    RaceContract.RaceEntry.COLUMN_NAME_DATE + " TEXT," +
                    RaceContract.RaceEntry.COLUMN_NAME_DISTANCE + " TEXT," +
                    RaceContract.RaceEntry.COLUMN_NAME_TIME + " TEXT," +
                    RaceContract.RaceEntry.COLUMN_NAME_LOCATION + " TEXT)"
    }

    var racesList: MutableList<RaceItem>
    private var resultId: Int = 0
    private var resultType: String = ""
    private var resultDate: String = ""
    private var resultDistance: String = ""
    private var resultTime: String = ""
    private var resultLocation: String = ""

    init {
        racesList = mutableListOf<RaceItem>()
    }

    /**
     *
     */
    override fun onCreate(db: SQLiteDatabase?) {
        // TODO("Not yet implemented")
        // creating table with fields

        db?.execSQL(SQL_CREATE_ENTRIES)

    }

    /**
     *
     */
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_RACES)
        onCreate(db)
    }

    /**
     *
     */
    override fun onDowngrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int): Unit {
        onUpgrade(db, oldVersion, newVersion)
    }
    /**
     * method to add a RaceItem to the SQLite Database
     */
    fun addRaceHandler(tool: RaceItem): Long {
        Log.v(RaceDbHelper.TAG, "Opening the Local Db for writing...")
        val db = this.writableDatabase
        Log.v(RaceDbHelper.TAG, "Db open!")
        val values = ContentValues()
        values.put(RaceContract.RaceEntry.COLUMN_NAME_TYPE, tool.raceType.toString())
        values.put(RaceContract.RaceEntry.COLUMN_NAME_DATE, tool.raceDate)
        values.put(RaceContract.RaceEntry.COLUMN_NAME_DISTANCE, tool.raceDistance)
        values.put(RaceContract.RaceEntry.COLUMN_NAME_TIME, tool.raceTime)
        values.put(RaceContract.RaceEntry.COLUMN_NAME_LOCATION, tool.raceLocation)
        Log.v(RaceDbHelper.TAG, "Adding race entry to Db...")
        val newRowId = db.insert(RaceContract.RaceEntry.TABLE_NAME, null, values)
        Log.v(RaceDbHelper.TAG, "Entry $newRowId added to Db")
        db.close()
        return newRowId
    }

    /**
     *
     */
    fun findAllRaces(): MutableList<RaceItem> {
        val db = readableDatabase

        //Define a projection that specifies which columns from the database
        // you wll actually use after this query.
        val projection = arrayOf(
            RaceContract.RaceEntry._ID,
            RaceContract.RaceEntry.COLUMN_NAME_TYPE,
            RaceContract.RaceEntry.COLUMN_NAME_DATE,
            RaceContract.RaceEntry.COLUMN_NAME_DISTANCE,
            RaceContract.RaceEntry.COLUMN_NAME_TIME,
            RaceContract.RaceEntry.COLUMN_NAME_LOCATION
        )
        // Filter results WHERE :
        val selection = RaceContract.RaceEntry._ID + " = ?"
        val selectionArgs = arrayOf<String>()

        // How you want the results sorted int he resulting Cursor
        val sortOrder = RaceContract.RaceEntry.COLUMN_NAME_TIME + " DESC"
        val result = db.query(
            RaceContract.RaceEntry.TABLE_NAME,  // The table you want to query
            projection,  // The columns you want to return
            null,  // the columns you want to return
            null,  // the columns for the WHERE clause
            null,  // the values for the WHERE clause
            null,  // do not group the rows
            sortOrder
        )

        val racesList = mutableListOf<RaceItem>()

        if (result.moveToFirst()) {
            do {
                resultId = result.getInt(result.getColumnIndex(RaceContract.RaceEntry._ID))
                resultType =
                    result.getString(result.getColumnIndex(RaceContract.RaceEntry.COLUMN_NAME_TYPE))
                resultDate =
                    result.getString(result.getColumnIndex(RaceContract.RaceEntry.COLUMN_NAME_DATE))
                resultDistance =
                    result.getString(result.getColumnIndex(RaceContract.RaceEntry.COLUMN_NAME_DISTANCE))
                resultTime =
                    result.getString(result.getColumnIndex(RaceContract.RaceEntry.COLUMN_NAME_TIME))
                resultLocation =
                    result.getString(result.getColumnIndex(RaceContract.RaceEntry.COLUMN_NAME_LOCATION))

                val type = RACE_TYPES.valueOf(resultType)

                val race = RaceItem(
                    type,
                    resultDate,
                    resultDistance,
                    resultTime,
                    resultLocation
                )

                racesList.add(race)

            } while (result.moveToNext())
        }
        return racesList
    }


    fun findRacesByType(raceType: RACE_TYPES): MutableList<RaceItem> {
        val db = readableDatabase
        racesList = mutableListOf<RaceItem>()

        //Define a projection that specifies which columns from the database
        // you wll actually use after this query.
        val projection = arrayOf(
            RaceContract.RaceEntry._ID,
            RaceContract.RaceEntry.COLUMN_NAME_TYPE,
            RaceContract.RaceEntry.COLUMN_NAME_DATE,
            RaceContract.RaceEntry.COLUMN_NAME_DISTANCE,
            RaceContract.RaceEntry.COLUMN_NAME_TIME,
            RaceContract.RaceEntry.COLUMN_NAME_LOCATION
        )
        // Filter results WHERE "type" = et_ToolType entry
        val selection = RaceContract.RaceEntry.COLUMN_NAME_TYPE + " = ?"
        val selectionArgs = arrayOf(raceType.toString())

        // How you want the results sorted int he resulting Cursor
        val sortOrder = RaceContract.RaceEntry.COLUMN_NAME_DISTANCE + " DESC"
        val result = db.query(
            RaceContract.RaceEntry.TABLE_NAME,  // The table you want to query
            projection,  // The columns you want to return
            selection,  // the columns you want to return
            selectionArgs,  // the columns for the WHERE clause
            null,  // the values for the WHERE clause
            null,  // do not group the rows
            sortOrder
        )

        if (result.moveToFirst()) {
            do {
                resultId = result.getInt(result.getColumnIndex(RaceContract.RaceEntry._ID))
                resultType =
                    result.getString(result.getColumnIndex(RaceContract.RaceEntry.COLUMN_NAME_TYPE))
                resultDate =
                    result.getString(result.getColumnIndex(RaceContract.RaceEntry.COLUMN_NAME_DATE))
                resultDistance =
                    result.getString(result.getColumnIndex(RaceContract.RaceEntry.COLUMN_NAME_DISTANCE))
                resultTime =
                    result.getString(result.getColumnIndex(RaceContract.RaceEntry.COLUMN_NAME_TIME))
                resultLocation =
                    result.getString(result.getColumnIndex(RaceContract.RaceEntry.COLUMN_NAME_LOCATION))

                val type = RACE_TYPES.valueOf(resultType)

                val race = RaceItem(
                    type,
                    resultDate,
                    resultDistance,
                    resultTime,
                    resultLocation
                )

                racesList.add(race)

            } while (result.moveToNext())
        }
        return racesList
    }


    fun elvDetails(): HashMap<RACE_TYPES, List<RaceItem>> {
        val db = readableDatabase
        val cursor = findAllRaces()

        var elvDetails = HashMap<RACE_TYPES, List<RaceItem>>()
        var elvRaces = mutableListOf<RaceItem>()
        var elvRuns= mutableListOf<RaceItem>()
        var elvWalks= mutableListOf<RaceItem>()
        var elvSprints= mutableListOf<RaceItem>()
        var elvCross= mutableListOf<RaceItem>()
        var elvFreeruns= mutableListOf<RaceItem>()
        var elvNones= mutableListOf<RaceItem>()

        cursor.forEach {
            //if (it.raceType == RACE_TYPES.RACE) elvRaces.add(it)
            when(it.raceType) {
                RACE_TYPES.RACE -> elvRaces.add(it)
                RACE_TYPES.WALK -> elvWalks.add(it)
                RACE_TYPES.SPRINT -> elvSprints.add(it)
                RACE_TYPES.RUN -> elvRuns.add(it)
                RACE_TYPES.CROSS -> elvCross.add(it)
                RACE_TYPES.FREERUN -> elvFreeruns.add(it)
                else -> elvNones.add(it)
            }
        }

        elvDetails.put(RACE_TYPES.RACE, elvRaces)
        elvDetails.put(RACE_TYPES.WALK, elvWalks)
        elvDetails.put(RACE_TYPES.SPRINT, elvSprints)
        elvDetails.put(RACE_TYPES.RUN, elvRuns)
        elvDetails.put(RACE_TYPES.CROSS,elvCross)
        elvDetails.put(RACE_TYPES.FREERUN, elvFreeruns)

        return elvDetails
    }

    /**
     *
     */
    fun elvTitles(): List<String> {
        var typesList = mutableListOf<String>()
        //typesList = RACE_TYPES.values()
        for(type in RACE_TYPES.values()){
            typesList.add(type.name)
        }
        return typesList
    }
}