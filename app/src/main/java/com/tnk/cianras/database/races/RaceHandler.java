package com.tnk.cianras.database.races;

/**
 * Created by Tom on 2017-12-29.
 *
 * @TRY
 * I think I have to make this into a AsyncTask in order to prevent hangups
 */



import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;

import com.tnk.cianras.database.location.RACE_TYPES;

public class RaceHandler extends SQLiteOpenHelper  {

    //NEW TO TRY
    // FIXME Database Entries
    // FIXME Database Entries

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + RaceContract.RaceEntry.TABLE_NAME + " (" +
                    RaceContract.RaceEntry._ID + " INTEGER PRIMARY KEY," +
                    RaceContract.RaceEntry.COLUMN_NAME_TYPE + " TEXT," +
                    RaceContract.RaceEntry.COLUMN_NAME_DATE + " TEXT," +
                    RaceContract.RaceEntry.COLUMN_NAME_DISTANCE + " TEXT," +
                    RaceContract.RaceEntry.COLUMN_NAME_TIME + " TEXT," +
                    RaceContract.RaceEntry.COLUMN_NAME_LOCATION + " TEXT)";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + RaceContract.RaceEntry.TABLE_NAME;
    // FIXME Database Entries

    //information of database
    private static final int DATABASE_VERSION = 0;
    private static final String DATABASE_NAME = "racesDB.db";
    public static final String TABLE_NAME = "Races";
    public static final String COLUMN_ID = "RaceID";
    public static final String COLUMN_TYPE = "RaceType";
    public static final String COLUMN_DATE = "RaceDate";
    public static final String COLUMN_DISTANCE = "RaceDistance";
    public static final String COLUMN_TIME = "RaceTime";
    public static final String COLUMN_LOCATION = "RaceLocation";

    //initialize the database

    public RaceHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override

    public void onCreate(SQLiteDatabase db) {
        // FIXME Database Entries

        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_ID + "INTEGER PRIMARYKEY," +
                COLUMN_TYPE + "TEXT," +
                COLUMN_DATE + "TEXT," +
                COLUMN_DISTANCE + "TEXT," +
                COLUMN_TIME + "TEXT," +
                COLUMN_LOCATION + "TEXT" +
                " )";
        db.execSQL(CREATE_TABLE);

    }
    @Override

    public void onUpgrade(SQLiteDatabase db, int i, int i1) {}
    public String loadHandler() {
        String result = "";
        String query = "Select*FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            int result_0 = cursor.getInt(0);
            String result_1 = cursor.getString(1);
            result += result_0 + " " + result_1 +
                    System.getProperty("line.separator");

        }
        cursor.close();
        db.close();
        return result;
    }

    // FIXME Database Entries
    public void addHandler(RaceItem tool) {
        ContentValues values = new ContentValues();
        //values.put(COLUMN_ID, tool.getID());
        values.put(COLUMN_TYPE, tool.getRaceType().toString());
        values.put(COLUMN_DATE, tool.getRaceDate());
        values.put(COLUMN_DISTANCE, tool.getRaceDistance());
        values.put(COLUMN_TIME, tool.getRaceTime());
        values.put(COLUMN_LOCATION, tool.getRaceLocation());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME, null, values);
        db.close();

    }

    public RaceItem findByNameHandler(String toolName) {

        String query = "Select * FROM " + TABLE_NAME + "WHERE" + COLUMN_DATE + " = " + "'" + toolName + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        RaceItem tool = new RaceItem(RACE_TYPES.NONE);       // default ID

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            tool.setRaceId(Integer.parseInt(cursor.getString(0)));
            tool.setRaceDate(cursor.getString(1));
            cursor.close();
        } else {
            tool = null;
        }
        db.close();
        return tool;
    }

    public boolean deleteByIDHandler(int ID) {
        boolean result = false;
        String query = "Select*FROM" + TABLE_NAME + "WHERE" + COLUMN_ID + "= '" + ID + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        RaceItem tool = new RaceItem(RACE_TYPES.NONE);
        if (cursor.moveToFirst()) {
            tool.setRaceId(Integer.parseInt(cursor.getString(0)));
            db.delete(TABLE_NAME, COLUMN_ID + "=?",
                    new String[] {
                String.valueOf(tool.getRaceId())
            });
            cursor.close();
            result = true;
        }
        db.close();
        return result;
    }

    public boolean deleteByDateHandler(String entryDate) {
        boolean result = false;
        String query = "Select*FROM" + TABLE_NAME + "WHERE" + COLUMN_DATE + "= '" + entryDate + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        RaceItem tool = new RaceItem(RACE_TYPES.NONE);
        if (cursor.moveToFirst()) {
            tool.setRaceDate(entryDate);
            db.delete(TABLE_NAME, COLUMN_DATE + "=?",
                    new String[] {
                            tool.getRaceDate()
                    });
            cursor.close();
            result = true;
        }
        db.close();
        return result;
    }

    public boolean updateByIDHandler(int ID, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(COLUMN_ID, ID);
        args.put(COLUMN_DATE, name);
        return db.update(TABLE_NAME, args, COLUMN_ID + "=" + ID, null) > 0;
    }

    /*
    @TODO
    Will work, would like to find a way to just was the ItemTool class around and have the handlers
    figure everything else out

    Have a main handler, with a boolean or INT tag that sorts the info of the ItemTool class after
    the caller sends it here
     */

    public boolean updateByNameHandler(String type,
                                       String date,
                                       String distance,
                                       String time,
                                       String location) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(COLUMN_TYPE, type);
        args.put(COLUMN_DATE, date);
        args.put(COLUMN_DISTANCE, distance);
        args.put(COLUMN_TIME, time);
        args.put(COLUMN_LOCATION, location);
        /*
        @TODO
        Catch that the 'name' is in the db before hand
         */
        return db.update(TABLE_NAME, args, COLUMN_DATE + "= '" + date + "'", null) > 0;
    }
}
