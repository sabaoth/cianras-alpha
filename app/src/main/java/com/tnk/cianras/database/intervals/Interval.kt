package com.tnk.cianras.database.intervals

import android.os.CountDownTimer
import android.util.Log
import com.tnk.cianras.database.intervals.DataIntervals

class Interval(type: DataIntervals.INTERVALTYPES,
                duration: Long) {

    var name: String = ""
    var intervalType: DataIntervals.INTERVALTYPES = DataIntervals.INTERVALTYPES.JOG
    var intervalDuration: Long = 0

    init{
        this.name = "TIMER_" + type
        this.intervalType = type
        this.intervalDuration = duration
    }
}