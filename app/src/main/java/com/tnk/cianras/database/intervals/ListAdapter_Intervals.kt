package com.tnk.cianras.database.intervals

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.tnk.cianras.R
import com.tnk.cianras.database.intervals.Interval

class ListAdapter_Intervals(private val context: Context,
                            private val dataSource: MutableList<Interval>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val rowView = inflater.inflate(R.layout.item_list_interval, parent, false)

        val tvName = rowView.findViewById<TextView>(R.id.tv_name)
        val tvType = rowView.findViewById<TextView>(R.id.tv_type)
        val tvDuration = rowView.findViewById<TextView>(R.id.tv_duration)

        val interval = getItem(position) as Interval
        tvName.text = interval.name
        tvType.text = interval.intervalType.toString()
        tvDuration.text = interval.intervalDuration.toString()

        return rowView
    }
}