package com.tnk.cianras.database.location

import android.content.Context
import android.location.Location
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.tnk.cianras.R
import org.w3c.dom.Text

class ListAdapter_Location(private val context: Context,
                           private val dataSource: MutableList<Location>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.item_list_location, parent, false)

        val tvLat = rowView.findViewById<TextView>(R.id.tv_lat)
        val tvLong = rowView.findViewById<TextView>(R.id.tv_long)
        val tvAlt = rowView.findViewById<TextView>(R.id.tv_alt)
        val tvBear = rowView.findViewById<TextView>(R.id.tv_bearing)

        val locale = getItem(position) as Location
        tvLat.text = locale.latitude.toString()
        tvLong.text = locale.longitude.toString()
        tvAlt.text = locale.altitude.toString()
        tvBear.text = locale.bearing.toString()

        return rowView
    }
}