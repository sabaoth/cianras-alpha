package com.tnk.cianras.database.events

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log
import com.tnk.cianras.database.location.DataLocation
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread

class EventSoundURI(context: Context,
                    distance: Double,
                    sound: String) {

    private val TAG: String = "EventSound :: "

    private var dLocation: DataLocation =
        DataLocation

    var triggerDistance: Double = 0.0
    var soundResource: String = ""
    var mediaPlayer: MediaPlayer? = null
    var callerContext: Context? = null
    var isTriggered: Boolean = false

    companion object;

    init {
        this.triggerDistance = distance
        this.soundResource = sound
        this.callerContext = context
    }

    fun loadMediaPlayer() {
        // Start the async call!
        async {
            Log.v(TAG, "async init")
            while(dLocation.mTotalDistance <= triggerDistance)
            {
                isTriggered = false
            }
            // Init the MediaPlayer
            mediaPlayer = MediaPlayer().apply {
                setAudioStreamType(AudioManager.STREAM_MUSIC)
                val soundUri = Uri.parse(soundResource)
                setDataSource(soundResource)
                prepare()
                setOnPreparedListener {
                    Log.v(TAG, "Event at $triggerDistance m loaded!")
                }
                start()
            }
            isTriggered = true
            uiThread {
                Log.v(TAG, "Event at $triggerDistance m played!")
                //mediaPlayer.stop()
                //mediaPlayer.seekTo(0)
                // = "mps $triggerDistance playing"

                // Release and Resolve the MediaPlayer
                mediaPlayer?.release()
                mediaPlayer = null
            }
        }
    }

}