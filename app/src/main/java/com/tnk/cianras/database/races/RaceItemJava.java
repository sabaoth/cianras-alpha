package com.tnk.cianras.database.races;

/**
 * Created by Tom on 2017-12-29.
 */

public class RaceItemJava {

    // fields
    private int raceID;
    private String raceType;
    private String raceName;
    private String raceDistance;
    private String raceTime;
    private String raceLocation;
    private String raceNote;
    private String racePlayer;
    private String raceWinner;
    private String raceAccolades;
    private String raceRating;
    private String raceStatus;
    private String raceCat;

    // constructors
    public RaceItemJava() {
    }

    public RaceItemJava(int id,
                        String raceType,
                        String raceName,
                        String raceDistance,
                        String raceTime,
                        String raceLocation,
                        String raceNote,
                        String racePlayer,
                        String raceWinner,
                        String raceAccolades,
                        String raceRating,
                        String raceStatus,
                        String raceCat
    ) {
        this.raceID = id;
        this.raceType = raceType;
        this.raceName = raceName;
        this.raceDistance = raceDistance;
        this.raceTime = raceTime;
        this.raceLocation = raceLocation;
        this.raceNote = raceNote;
        this.racePlayer = racePlayer;
        this.raceWinner = raceWinner;
        this.raceAccolades = raceAccolades;
        this.raceRating = raceRating;
        this.raceStatus = raceStatus;
        this.raceCat = raceCat;
    }

    // #SET/#GET      Properties of the Class        HERE
    public void setID(int id) {this.raceID = id;}
    public int getID() {return this.raceID;}
    public void setRaceType(String type) {this.raceName = type;}
    public String getRaceType() {return this.raceType;}
    public void setRaceName(String name) {this.raceName = name;}
    public String getRaceName() {return this.raceName;}
    public void setRaceDistance(String brand) {this.raceDistance = brand;}
    public String getRaceDistance() {return this.raceDistance;}
    public void setRaceTime(String quantity) {this.raceTime = quantity;}
    public String getRaceTime() {return this.raceTime;}
    public void setRaceLocation(String quality) {this.raceLocation = quality;}
    public String getRaceLocation() {return this.raceLocation;}
    public void setRaceNote(String location) {this.raceNote = location;}
    public String getRaceNote() {return this.raceNote;}

    public void setRacePlayer(String pic) {this.racePlayer = pic;}
    public String getRacePlayer() {return this.racePlayer;}
    public void setRaceWinner(String size) {this.raceWinner = size;}
    public String getRaceWinner() {return this.raceWinner;}
    public void setRaceAccolades(String uses) {this.raceAccolades = uses;}
    public String getRaceAccolades() {return this.raceAccolades;}
    public void setRaceRating(String ammo) {this.raceRating = ammo;}
    public String getRaceRating() {return this.raceRating;}
    public void setRaceStatus(String category) {this.raceStatus = category;}
    public String getRaceStatus() {return this.raceStatus;}
    public void setRaceCat(String status) {this.raceCat = status;}
    public String getRaceCat() {return this.raceCat;}

}