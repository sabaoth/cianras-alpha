package com.tnk.cianras

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.location.Location
import android.location.LocationListener
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.tnk.cianras.database.location.DataLocation
import com.tnk.cianras.database.intervals.DataIntervals
import com.tnk.cianras.ui.intervals.FragmentRaceInterval
import com.tnk.cianras.ui.location.FragmentRaceLocation
import com.tnk.cianras.ui.progress.FragmentRaceProgress
import com.tnk.cianras.ui.timer.FragmentRaceTimer
import kotlinx.android.synthetic.main.obj_current_location.*

class FragmentRace : Fragment(), LocationListener, OnMapReadyCallback {

    // FIXME Launch this from the MainActivity once a RaceDistance has been selected

    // @LABEL BEGIN ACTIVITY VARIABLES
    internal val TAG: String = "FragmentRace :: "
    internal var initUI: Boolean = false

    /**
     * This is the companion object
     */
    companion object {
        var uiLocation: FragmentRaceLocation? = null
        var uiIntervals: FragmentRaceInterval? = null
        var uiProgress: FragmentRaceProgress? = null
        var uiTimer: FragmentRaceTimer? = null
        //var uiOverseer: Overseer? = null

        // LOCATION VARIABLES
        var LOCATION_UPDATES: Boolean = false
        private lateinit var mMap: GoogleMap
        private var mLatLng = LatLngBounds.Builder()
    }

    var dataIntervals: DataIntervals = DataIntervals
    //var dataLocations: DataLocation = DataLocation
    // Provides the entry point to the Fused Location Provider API
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    lateinit var mContext: AppCompatActivity

    // @LABEL END ACTIVITY VARIABLES
    // @LABEL BEGIN ACTIVITY REQUIREMENTS
    init {
        uiLocation = FragmentRaceLocation()
        uiIntervals = FragmentRaceInterval()
        uiProgress = FragmentRaceProgress()
        uiTimer = FragmentRaceTimer()
        //uiOverseer = Overseer()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.frag_race, container, false)
        //
        initUI(root)
        //
        return root
    }

    private fun initUI(root: View) {
        DataLocation.setContext(requireContext())
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        // @LABEL GOOGLE MAP
        val fm = mContext.supportFragmentManager
        val fragmentTransaction: FragmentTransaction
        val fragment = SupportMapFragment()
        fragmentTransaction = fm.beginTransaction()
        fragmentTransaction.replace(R.id.map, fragment)
            .addToBackStack(null)
        fragment.getMapAsync(this)
        fragmentTransaction.commit()

        startLocationUpdates()
        //
        initUI = true
    }

    // @LABEL END ACTIVITY REQUIREMENTS
    // @LABEL BEGIN LOCATION REQUIREMENTS
    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    override fun onProviderEnabled(provider: String?) {
        Toast.makeText(
            requireContext(), "Enabled new provider " + provider,
            Toast.LENGTH_SHORT
        ).show()
    }
    override fun onProviderDisabled(provider: String?) {
        Toast.makeText(
            requireContext(), "Disabled provider " + provider,
            Toast.LENGTH_SHORT
        ).show()
    }
    /**
     *
     */
    override fun onLocationChanged(location: Location?) {
        //locationUpdate(location!!, "onLocationChanged()")
    }
    private fun createLocationRequest(): LocationRequest {
        val locationRequest: LocationRequest = LocationRequest.create().apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }; return locationRequest
    }
    // @LABEL This LocationCallBack succeeds!
    /* My Attempt at a LocationCallback */
    private fun createLocationCallback(): LocationCallback {
        DataLocation.mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(task: LocationResult?) {
                task ?: return
                for (location in task.locations) {
                    //locationUpdate(location!!, "createLocationCallback()")
                }
            }
        }; return DataLocation.mLocationCallback
    }

    /* Stop the LocationUpdates for this Callback? */
    private fun stopLocationUpdate() {
        if(LOCATION_UPDATES) {
            mFusedLocationClient.removeLocationUpdates(createLocationCallback())
        }
        DataLocation.MAPPING = false
    }
    /* My attempt to start receiving location updates from the GPS Service */
    private fun startLocationUpdates() {
        /**
        if(initUI) {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            mFusedLocationClient.requestLocationUpdates(
                createLocationRequest(),
                createLocationCallback(),
                Looper.getMainLooper()
            )
            LOCATION_UPDATES = true
        }
        **/
        Log.v(TAG, "startLocationUpdates called")
    }
    /**
     * @author Sabaoth87
     *
     * My Attempt At A Location Worker
     * - updates UI
     * - adds location to the collection
     */
    private fun locationUpdate(
        location: Location,
        caller: String
    ) {
        Log.v(TAG, "Adding LOCATION from " + caller)
        // @REMARK MAPPING was required when waiting for GoogleMap to attach
        if (DataLocation.MAPPING) {
            Log.v(TAG, "MAPPING")
            // @LABEL Location && mMap
            val newLocale = LatLng(
                location.latitude,
                location.longitude
            )
            // This location is now the mCurrentLocation
            DataLocation.mCurrentLocation = location
            //  Has there been more than one location?
            if(DataLocation.mLastLocation != null) {
                Log.v(TAG, "mLastLocation Not Null!")
                val distanceTest = DataLocation.distanceBetween(
                    DataLocation.mCurrentLocation!!,
                    DataLocation.mLastLocation!!
                )
                // If the new location is far enough from the old location, add it
                if (distanceTest >= DataLocation.mDistanceFence) {
                    // TODO DEPRECATED for this Location View
                    // @REMARK Consider adding a Map Fragment
                    if(DataLocation.MAPPING) updateMap(newLocale)
                    // @LABEL Checking to see if RACE is PAUSED
                    // FIXME Ensure this will act as desired
                    /**
                     * INTENT
                     *  IF the location is 'far enough' away from the last location
                     */
                    DataLocation.mLocationsList.add(location)

                    if(!DataLocation.RACE_PAUSED) {
                        DataLocation.addLocation(location)
                    }

                    // After Location and Distance set, update the UI
                    //updateUI()
                    uiLocation!!.updateUI()
                }
            }
            // This location is  now the mLastLocation known to the process!
            DataLocation.mLastLocation = location
        }
        Log.v(TAG, "Locations " + DataLocation.mLocationsList.size)

        //mCurrentLocations.add(location)
        DataLocation.laLocations.notifyDataSetChanged()

        // @LABEL Location && mMap
        //val newLocale = LatLng(location.latitude, location.longitude)
        //mMap.addMarker(MarkerOptions().position(newLocale).title("."))
        //mMap.maxZoomLevel
        //FragmentLocation.newInstance().locationUpdate(location)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        this.mContext = context as AppCompatActivity
        startLocationUpdates()
    }
    /* Remove the locationlistener when the Activity is paused */
    override fun onPause() {
        super.onPause()
        stopLocationUpdate()
    }

    /* Remove the locationlistener when the Activity is destroyed */
    override fun onDestroy() {
        super.onDestroy()
        stopLocationUpdate()
    }

    override fun onDetach() {
        super.onDetach()
        stopLocationUpdate()
    }
    /* Request updates at startup */
    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if(!LOCATION_UPDATES) {
            startLocationUpdates()
            //mFusedLocationClient.requestLocationUpdates(this, 400, 1f, this)
        }
        //startLocationUpdates()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        //Checks the orientation of the screen
        if (newConfig.orientation === Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(requireContext(), "landscape", Toast.LENGTH_SHORT).show()
        } else if (newConfig.orientation === Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(requireContext(), "portrait", Toast.LENGTH_SHORT).show()
        }
    }

    // @LABEL BEGIN UI
    /**
     * Very messy means of updating the UI
     * @author Sabaoth87
     *
     *
     */
    private fun updateUI() {
        // FragmentLocation UI
        tv_cl_lat.text = DataLocation.mCurrentLocation!!.latitude.toString()
        tv_cl_long.text = DataLocation.mCurrentLocation!!.longitude.toString()
        tv_cl_distance.text = DataLocation.mTotalDistance.toString()
        // FragmentMinterface UI
    }
    // @LABEL END UI

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        Log.v(TAG, "onMapReady()")

        mMap = googleMap
        DataLocation.MAPPING = true
        // declare bounds object to fit whole route in screen
        // Add a marker in Sydney and move the camera
        val point1 = LatLng(44.346404, -77.335264)
        val point2 = LatLng(44.304893, -77.339508)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point1, 14.0f))
    }
    /**
     *
     */
    fun updateMap(locale: LatLng) {
        Log.v(TAG, "Location outside of fence")
        mMap.addMarker(MarkerOptions().position(locale).title(DataLocation.mLocationsList.size.toString()))
        mMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                locale,
                DataLocation.mZoomLevel
            )
        )
        //mLatLng.include(locale)
        //tv_min_distance.text = ataLocation
        //val bounds = mLatLng.build()
        //mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 1000))
        mMap.maxZoomLevel
    }
}
