package com.tnk.cianras

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceManager
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.navigation.NavigationView
import com.tnk.cianras.database.location.DataLocation
import com.tnk.cianras.database.location.DataLocation.mDistanceFence
import com.tnk.cianras.database.location.DataLocation.uiFence
import com.tnk.cianras.databinding.AppBarMainBinding
import com.tnk.cianras.ui.intervals.FragmentIntervals
import com.tnk.cianras.ui.location.FragmentLocation
import com.tnk.cianras.ui.minterface.FragmentMinterface
import kotlinx.android.synthetic.main.obj_current_location.*


class MainActivity : AppCompatActivity(), LocationListener {

    /**
     * This is the companion object
     */
    companion object {
        var navController: NavController? = null

        // Location Permission
        private const val REQUEST_CODE_PERMISSIONS = 87

        // Permission Array
        private val REQUIRED_PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
            //, Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS
            //, Manifest.permission.LOCATION_HARDWARE
        )

        /**
         * A preference value change listener that updates the preference's summary
         * to reflect its new value.
         */
        private val sBindPreferenceSummaryToValueListener =
            Preference.OnPreferenceChangeListener { preference, value ->

                val stringValue = value.toString()

                if (preference is ListPreference) {
                    // For list preferences, look up the correct display value in
                    // the preference's 'entries' list.
                    val listPreference = preference
                    val index = listPreference.findIndexOfValue(stringValue)

                    // Set the summary to reflect the new value.
                    preference.setSummary(
                        if (index >= 0)
                            listPreference.entries[index]
                        else
                            null)

                } else

                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.summary = stringValue
                true
            }

        private fun bindPreferenceSummaryToValue(preference: Preference) {
            // Set the listener to watch for value changes.
            preference.onPreferenceChangeListener = sBindPreferenceSummaryToValueListener

            // Trigger the listener immediately with the preference's
            // current value.
            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                    .getDefaultSharedPreferences(preference.context)
                    .getString(preference.key, ""))
        }
    }

    //
    private val TAG: String = "MainActivity :: "
    private lateinit var appBarConfiguration: AppBarConfiguration

    //
    var fragLocation: FragmentLocation
    var fragIntervals: FragmentIntervals
    var fragMinterface: FragmentMinterface

    // Provides the entry point to the Fused Location Provider API
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private lateinit var locationManager: LocationManager
    private var pDeveloperMode: Boolean = false
    private var pFence: Float = 5.0f
    private lateinit var tvFence: TextView
    var dataLocations: DataLocation = DataLocation
    private lateinit var prefs: SharedPreferences

    init {
        fragLocation = FragmentLocation.newInstance()
        fragIntervals = FragmentIntervals.newInstance()
        fragMinterface = FragmentMinterface.newInstance()
    }
    fun Float.format(digits: Int) = "%.${digits}f".format(this)
    fun Double.format(digits: Int) = "%.${digits}f".format(this)

    //@SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataLocations.setContext(this)
        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val binding: AppBarMainBinding = DataBindingUtil.setContentView(
            this, R.layout.app_bar_main)
        binding.dataLocation = dataLocations

        setContentView(R.layout.act_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        /**
        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        **/
        tvFence = findViewById(R.id.tv_debugging)

        pDeveloperMode = prefs.getBoolean("developer", false)
        mDistanceFence = prefs.getString("fence", "7.1").toDouble()
        uiFence = "Movement Fence set to ${mDistanceFence.format(2)}"

        Log.v(TAG, "Preferences loaded.")
        Log.v(TAG, "Developer mode " + pDeveloperMode)
        Log.v(TAG, "Fence is set to ${mDistanceFence.format(2)}")

        tvFence.text = "Fence is set to ${mDistanceFence.format(2)}"
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_race_settings,
                R.id.nav_race_current,
                R.id.nav_races_list,
                R.id.nav_settings,
                R.id.nav_debug_app
            ), drawerLayout
        )
        setupActionBarWithNavController(navController!!, appBarConfiguration)
        navView.setupWithNavController(navController!!)

        //navController!!.navigate(R.id.nav_race_current)

        // @LABEL LOCATION!!
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        //createLocationCallback()

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Log.v(TAG, "Permissions not granted!")
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        //locationManager =
        //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0.0f, this)
    }


    // @LABEL LocationListener Requirements
    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //
    override fun onProviderEnabled(provider: String?) {
        Toast.makeText(
            this, "Enabled new provider " + provider,
            Toast.LENGTH_SHORT
        ).show()
    }

    //
    override fun onProviderDisabled(provider: String?) {
        Toast.makeText(
            this, "Disabled provider " + provider,
            Toast.LENGTH_SHORT
        ).show()
    }
    // @LABEL LocationListener Requirements


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_act_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        Log.v(TAG, "Options Menu Selected " + item.toString())

        return if (item.toString() == "Settings") {
            Log.v(TAG, "Options Menu, Info!, Selected")
            //startActivity(Intent(this@MainActivity, SettingsActivity::class.java))
            //val intent = Intent(this, SettingsFragment::class.java).apply {
                //putExtra(EXTRA_MESSAGE, message)
            //}
            navController?.navigate(R.id.nav_settings)
            //startActivity(intent)
            true
        } else super.onOptionsItemSelected(item)
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    public override fun onStart() {
        super.onStart()
        if (allPermissionsGranted()) {
            startLocationUpdates()
            getLastLocation()

        } else {
            ActivityCompat.requestPermissions(
                this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS
            )
        }

        pDeveloperMode = prefs.getBoolean("developer", false)
        mDistanceFence = prefs.getString("fence", "7.1").toDouble()
        uiFence = "Movement Fence set to ${mDistanceFence.format(2)}"
    }

    /**
     * Check if all permission specified in the manifest have been granted
     */
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            baseContext, it
        ) == PackageManager.PERMISSION_GRANTED
    }

    /* Remove the locationlistener when the Activity is paused */
    override fun onPause() {
        super.onPause()
        stopLocationUpdate()
    }

    /* Remove the locationlistener when the Activity is destroyed */
    override fun onDestroy() {
        super.onDestroy()
        stopLocationUpdate()
    }

    /* Request updates at startup */
    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        //mFusedLocationClient.requestLocationUpdates(mProvider, 400, 1f, this)
        //if (receivingLocaionUpdates) startLocationUpdates()
        startLocationUpdates()
        getLastLocation()
        pDeveloperMode = prefs.getBoolean("developer", false)
        mDistanceFence = prefs.getString("fence", "7.1").toDouble()
        uiFence = "Movement Fence set to ${mDistanceFence.format(2)}"
    }
    // @LABEL LOCATION::START
    /**
     *
     */
    override fun onLocationChanged(location: Location?) {
        locationUpdate(location!!, "onLocationChanged()")
    }

    private fun createLocationRequest(): LocationRequest {
        val locationRequest: LocationRequest = LocationRequest.create().apply {
            interval = 500            // FIXME
            fastestInterval = 250      // @REMARK I believe this can be adjusted
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        builder.setAlwaysShow(true) //this is the key ingredient

        return locationRequest
    }

    // @LABEL This LocationCallBack succeeds!
    /* My Attempt at a LocationCallback */
    private fun createLocationCallback(): LocationCallback {
        dataLocations.mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(task: LocationResult?) {
                task ?: return
                for (location in task.locations) {
                    locationUpdate(location!!, "createLocationCallback()")
                }
            }
        }; return dataLocations.mLocationCallback
    }

    /* Stop the LocationUpdates for this Callback? */
    private fun stopLocationUpdate() {
        mFusedLocationClient.removeLocationUpdates(createLocationCallback())
        DataLocation.MAPPING = false
    }

    /* My attempt to start receiving location updates from the GPS Service */
    private fun startLocationUpdates() {
        if (allPermissionsGranted()) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            mFusedLocationClient.requestLocationUpdates(
                createLocationRequest(),
                createLocationCallback(),
                Looper.getMainLooper()
            )
            //getLastLocation()
        }
    }

    /**
     * Provides a simple way of getting a device's location and is well suited for
     * applications that do not require a fine-grained location and that do not need location
     * updates. Gets the best and most recent location currently available, which may be null
     * in rare cases when a location is not available.
     *
     *
     * Note: this method should be called after location permission has been granted.
     */
    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        mFusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    locationUpdate(location, "getLastLocation()")
                } else {
                    Log.w(TAG, "getLastLocation: Location null")
                    showMessage(getString(R.string.no_location_detected))
                }
            }
    }

    /**
     * @author Sabaoth87
     *
     * My Attempt At A Location Worker
     * - updates UI
     * - adds location to the collection
     */
    private fun locationUpdate(
        location: Location,
        caller: String
    ) {
        Log.v(TAG, "Adding LOCATION from " + caller)
        // @REMARK MAPPING was required when waiting for GoogleMap to attach
        if (dataLocations.MAPPING) {
            Log.v(TAG, "MAPPING")
            // @LABEL Location && mMap
            val newLocale = LatLng(
                location.latitude,
                location.longitude
            )
            // This location is now the mCurrentLocation
            DataLocation.mCurrentLocation = location
            //  Has there been more than one location?
            if (dataLocations.mLastLocation == null) {
                Log.v(TAG, "mLastLocation is NULL!!")
            }
             if (dataLocations.mLastLocation != null) {
                    Log.v(TAG, "mLastLocation Not Null!")
                    val distanceTest = dataLocations.distanceBetween(
                        dataLocations.mCurrentLocation!!,
                        dataLocations.mLastLocation!!
                    )
                    // If the new location is far enough from the old location, add it
                    if (distanceTest >= dataLocations.mDistanceFence) {
                        if (DataLocation.MAPPING) fragLocation.updateMap(newLocale)
                        // @LABEL Checking to see if RACE is PAUSED
                        // FIXME Ensure this will act as desired
                        // FIXME This does not work as desired
                        // Believe the code is adding a location when it should not.

                        /**
                         * INTENT
                         *  IF the location is 'far enough' away from the last location
                         */


                        if (!dataLocations.RACE_PAUSED) {
                            DataLocation.addLocation(location)
                        }

                        // After Location and Distance set, update the UI
                        updateUI()
                        //fragLocation.updateUI()
                    }
                }


            // This location is  now the mLastLocation known to the process!
            dataLocations.mLastLocation = location
        }
        Log.v(TAG, "Locations " + dataLocations.mLocationsList.size)

        //mCurrentLocations.add(location)
        dataLocations.laLocations.notifyDataSetChanged()

        // @LABEL Location && mMap
        //val newLocale = LatLng(location.latitude, location.longitude)
        //mMap.addMarker(MarkerOptions().position(newLocale).title("."))
        //mMap.maxZoomLevel
        //FragmentLocation.newInstance().locationUpdate(location)
    }

    /**
     * Very messy means of updating the UI
     * @author Sabaoth87
     *
     *
     */
    private fun updateUI() {
        // FragmentLocation UI
        tv_cl_lat.text = DataLocation.mCurrentLocation!!.latitude.toString()
        tv_cl_long.text = DataLocation.mCurrentLocation!!.longitude.toString()
        tv_cl_distance.text = DataLocation.mTotalDistance.toString()
        // FragmentMinterface UI
    }
    // LOCATION::END

    /**
     * Shows a [] using `text`.
     * @param text The Snackbar text.
     */
    private fun showMessage(text: String) {
        val container = findViewById<View>(R.id.frag_home)
        if (container != null) {
            Toast.makeText(this@MainActivity, text, Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Shows a [].
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * *
     * @param actionStringId   The text of the action item.
     * *
     * @param listener         The listener associated with the Snackbar action.
     */
    private fun showSnackbar(
        mainTextStringId: Int,
        actionStringId: Int,
        listener: View.OnClickListener
    ) {
        Toast.makeText(this@MainActivity, getString(mainTextStringId), Toast.LENGTH_LONG).show()
    }

    /**
     * Process result from permission request dialog box, has the request
     * been granted? If yes, start Location Gathering. Otherwise display a toast
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {


        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            // @REMARK This is the start of the location callback request
            mFusedLocationClient.requestLocationUpdates(
                createLocationRequest(),
                createLocationCallback(),
                Looper.getMainLooper()
            )
            return
        } else {
            Toast.makeText(
                this,
                "Permissions not granted by the user.",
                Toast.LENGTH_LONG
            ).show()
            finish()

            showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                View.OnClickListener {
                    // Build intent that displays the App settings screen.
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts(
                        "package",
                        BuildConfig.APPLICATION_ID, null
                    )
                    intent.data = uri
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                })
        }


        /**
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
        if (allPermissionsGranted()) {
        // @REMARK Permissions have been granted!
        startLocationUpdates()

        } else {
        Toast.makeText(
        this,
        "Permissions not granted by the user.",
        Toast.LENGTH_LONG
        ).show()
        finish()

        showSnackbar(R.string.permission_denied_explanation, R.string.settings,
        View.OnClickListener {
        // Build intent that displays the App settings screen.
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts(
        "package",
        BuildConfig.APPLICATION_ID, null
        )
        intent.data = uri
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        })
        }
         **/
    }
}
