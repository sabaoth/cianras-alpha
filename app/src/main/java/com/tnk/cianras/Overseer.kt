package com.tnk.cianras

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import com.tnk.cianras.database.location.DataLocation
import com.tnk.cianras.database.events.DistanceTrigger
import com.tnk.cianras.database.events.EventSoundURI
import com.tnk.cianras.database.events.EventString
import com.tnk.cianras.database.location.RACE_TYPES
import com.tnk.cianras.database.races.RaceDbHandler
import com.tnk.cianras.database.races.RaceItem
import java.util.*

class Overseer : Fragment(), TextToSpeech.OnInitListener {


    private var dLocations: DataLocation =
        DataLocation
    private var ttsEnabled: Boolean = false
    var mps = mutableListOf<MediaPlayer>()
    var raceSounds = mutableListOf<DistanceTrigger>()
    var raceCaptions = mutableListOf<EventString>()
    var raceSoundURIs = mutableListOf<EventSoundURI>()
    var raceDistanceEvent = mutableListOf<DistanceTrigger>()

    var distance: Double = 0.0
    var duration: Int = 0
    var soundInt: Int = 0
    var isTriggered: Boolean = false
    var soundUri: String = ""


    //private var eventStrEncouragements: Array<String>? = null
    //private var eventStrOpenings: Array<String>? = null
    //private var eventStrWeatherSunny: Array<String>? = null
    //private var eventStrWeatherRaining: Array<String>? = null
    //private var eventStrSoundURIs: Array<String>? = null

    companion object {
        private val TAG: String = "Overseer :: "

        var tvCaptions: TextView? = null
        var mTts: TextToSpeech? = null

        var eventStrEncouragements: Array<String>? = null
        var eventStrOpenings: Array<String>? = null
        var eventStrWeatherSunny: Array<String>? = null
        var eventStrWeatherRaining: Array<String>? = null
        var eventStrSoundURIs: Array<String>? = null
        var eventStr100meters: Array<String>? = null
        var eventStrKilometers: Array<String>? = null
        var eventStr5minutes: Array<String>? = null
        var listEventsDistance: MutableList<EventDistance>? = null
        var listCapsDistance: MutableList<CaptionDistance>? = null
        var listEventsTime: MutableList<EventTime>? = null
        var triggerIndexDistance: Int = 0
        var triggerIndexTime: Int = 0
        var mHandler: Handler? = null
        var triggerDistance: Double = 0.0
        var triggerTime: Long = 0
        var mediaPlayer: MediaPlayer? = null
        var callerTV: TextView? = null
        var callerContext: Context? = null
        var captionText: String = ""
        var nextDistance = 0.0

        // @LABEL TTS Function
        fun speakOut(speech: String) {
            // speaking
            Log.v(TAG, "Saying: $speech")
            mTts!!.speak(speech, TextToSpeech.QUEUE_FLUSH, null, "")
        }
        private fun updateNextDistance() {
            if(triggerIndexDistance < listCapsDistance!!.size) {
                nextDistance = listCapsDistance!![triggerIndexDistance].distance
            }
            Log.v(TAG, "Next Distance Event @ $nextDistance")
        }
    }

    private var soundList = mutableListOf<Int>(
        R.raw.running_ice,
        R.raw.running_snow,
        R.raw.chant1,
        R.raw.chant2,
        R.raw.ping,
        R.raw.llama,
        R.raw.demaru_drum,
        R.raw.tibetan_bell0,
        R.raw.tibetan_bell1,
        R.raw.ping_bullet,
        R.raw.ping_metal1,
        R.raw.ping_metal2,
        R.raw.ping_metal3
    )

    init {
        Log.v(TAG, "init BEGINS")
        listEventsDistance = mutableListOf<EventDistance>()
        listEventsTime = mutableListOf<EventTime>()
        listCapsDistance = mutableListOf<CaptionDistance>()
        mHandler?.postDelayed(TriggerListener, 0)
        Log.v(TAG, "init ENDS")
    }

    object TriggerListener : Runnable {
        override fun run() {
            //this is where we check for triggers

            //DISTANCE
            //if(DataLocation.mTotalDistance > listEventsDistance!![triggerIndexDistance].distance)
            if (DataLocation.mTotalDistance > nextDistance) {

                if(triggerIndexDistance < listCapsDistance!!.size) {
                    captionText = listCapsDistance!![triggerIndexDistance].string
                }
                tvCaptions?.text = captionText
                speakOut(captionText)
                //Log.v(TAG, "Event at $distance m played!")
                //mediaPlayer.stop()
                //mediaPlayer.seekTo(0)
                // = "mps $triggerDistance playing"
                // Release and Resolve the MediaPlayer
                //mediaPlayer?.release()
                //mediaPlayer = null
                //isTriggered = false
                triggerIndexDistance++
                updateNextDistance()
            }// if*
            //TIME
            //if(DataLocation.)
            //REPEAT
            mHandler?.postDelayed(this, 0)
        }
    }



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.act_overseer, container, false)
        // NEW
        // NEW
        initUI(root)
        //loadMediaPlayers()
        //playMediaPlayers()
        //loadEvents()

        //dLocations.loadCaptionsByDistanceExample()
        //loadCaptionsByDistanceExample()
        //loadSoundURIsByDistanceExample()

        addRace()
        return root
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            // set Canadian English as language for tts
            val result = mTts!!.setLanguage(Locale.CANADA)

            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e(TAG, "The Language specified is not supported")
            } else {
                ttsEnabled = true
            }

        } else {
            Log.e(TAG, "TTS Initialization Failed!")
        }
    }

    private fun eventDistanceLoad() {
        // FIXME CONVERT TO PROPER raw RESOURCES!
        // @LABEL String Events!
        var eventRunner: CaptionDistance? = null

        for ((index, string) in eventStr100meters!!.withIndex()) {
            eventRunner = CaptionDistance(
                (index * 100.0) + 100,
                string
            )
            listCapsDistance!!.add(eventRunner)
        }
    }

    /**
     *
     */
    private fun stopAll() {
        for (mp in mps) mp.release()
    }

    /**
     * Initialize the User Interface
     *
     */
    private fun initUI(root: View) {
        mTts = TextToSpeech(requireContext(), this)

        tvCaptions = root.findViewById(R.id.tv_caption)
        // ++ Resource Arrays ++ //
        eventStrEncouragements = resources.getStringArray(R.array.encouragements)
        eventStrOpenings = resources.getStringArray(R.array.openings)
        eventStrWeatherSunny = resources.getStringArray(R.array.weather_sunny)
        eventStrWeatherRaining = resources.getStringArray(R.array.weather_raining)
        eventStrSoundURIs = resources.getStringArray(R.array.sound_uri)
        eventStr100meters = resources.getStringArray(R.array.distance_100meters)
        eventStrKilometers = resources.getStringArray(R.array.distance_kilometers)
        eventStr5minutes = resources.getStringArray(R.array.time_5minutes)
        mHandler = Handler()
        mHandler?.postDelayed(TriggerListener, 0)

        eventDistanceLoad()
        updateNextDistance()
    }

    // @LABEL LIFECYCLE
    override fun onDetach() {
        if (mTts != null) {
            mTts!!.stop()
            mTts!!.shutdown()
        }
        super.onDetach()
        Log.v(TAG, "onDetach()")
    }

    override fun onDestroyView() {
        if (mTts != null) {
            mTts!!.stop()
            mTts!!.shutdown()
        }
        super.onDestroyView()
        Log.v(TAG, "onDestroyView()")
    }
    // @LABEL LIFECYCLE

    // @LABEL START DATA PERSISTENCE START
    // @FIXME RequiresApi
    fun addRace() {
        //Log.v(TAG, "Adding this race to he database!")
        val dbHandler = RaceDbHandler(requireContext())
        val date = Calendar.getInstance()

        async {
            Log.v(TAG, "async init")
            while (!DataLocation.RACE_COMPLETED) {
                // do nothing
                //dLocations.RACE_COMPLETED = false
            }
            Log.v(TAG, "Race Distance Met!")


            uiThread {
                Log.v(TAG, "addRace()")
                val newRace = RaceItem(
                    // FIXME Dynamically select the RACE_TYPE
                    RACE_TYPES.FREERUN,
                    date.time.toString(),
                    dLocations.mTotalDistance.toString(),
                    SystemClock.elapsedRealtime().toString(),
                    "${dLocations.mLocationsList[0].latitude}" +
                            "_" +
                            "${dLocations.mLocationsList[0].longitude}"
                )
                dbHandler.addRaceHandler(newRace)
                //dLocations.RACE_COMPLETED = true
                //dLocations.RACE_COMPLETE = true
                //dLocations.RACE_PAUSED = true
            }
        }
    }
    // @LABEL END DATA PERSISTENCE END

    // @LABEL AudioPipeline Development

    //
    class EventTime {
        internal var time: Long = 0
        internal var sound: Int = 0
    }

    //
    class EventDistance(
        inDistance: Double,
        inSound: Int
    ) {
        internal var distance: Double = inDistance
        internal var sound: Int = inSound
    }

    class CaptionDistance (
        inDistance: Double,
        inString: String
    ){
        internal var distance: Double = inDistance
        internal var string: String = inString
    }

}

