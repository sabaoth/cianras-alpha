package com.tnk.cianras

import android.content.Context
import android.media.MediaPlayer
import android.os.Handler
import android.util.Log
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.tnk.cianras.database.location.DataLocation
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread

object AudioPipeline: Fragment() {

    private val TAG: String = "DistanceTrigger :: "
    //
    private var dLocation: DataLocation =
        DataLocation
    var mediaPlayer: MediaPlayer? = null
    var callerTV: TextView? = null
    var callerContext: Context? = null
    var captionText: String = ""
    var distance: Double = 0.0
    var duration: Int = 0
    var soundInt: Int = 0
    var isTriggered: Boolean = false
    var soundUri: String = ""
    var triggerDistance: Double = 0.0
    var triggerTime: Long = 0
    var triggerIndexDistance: Int = 0
    var triggerIndexTime: Int = 0
    var mHandler: Handler? = null
    //var listEventsDistance: MutableList<eventDistance>? = null
    //var listEventsTime: MutableList<eventTime>? = null
    //
    init {
        Log.v(TAG, "init")
        mHandler = Handler()
        //listEventsDistance = mutableListOf<eventDistance>()
        //listEventsTime = mutableListOf<eventTime>()

        //mHandler?.postDelayed(triggerListener, 0)
    }

}

