#Cianrás

Inspired by the Irish, Cianrás: distance race

---

## Cianrás(alpha)
Android Studio 3.6.1
API: Build@28 min@21
Kotlin 1.3.61
---

## Intervals    :: IN PROGRESS
To support interval training, a set of instance durations can be created and iterated in a sequence
with audible and UI cues to signal the transition from interval to interval.
## Pathing      :: IN PROGRESS
User paths will be captured and analyzed to determine relevant stats and audible cues.
Utilizing
https://medium.com/@manuaravindpta/getting-current-location-in-kotlin-30b437891781
as a starting point.
## Start a Run  :: IN PROGRESS
Select a distance, hit start, and the program takes over.
The application ends without disabling the location callbacks.

---

###### ToDo
* [ ] look into streaming the sounds
    * save eventual build size / time
    * easily updatable
* [ ] improve pathing
* [ ] finish Settings 
* [o] create a run
    * [x] Overall Run Timer
    * [ ] Overall Run Route / Path
        * Distance
        * Time
* [ ] housekeeping
    * [ ] project structure
    * [ ] resource files
* [o] create audio pipeline
    Sounds currently can be triggered by 'waiting' for a distance to be met
    Can queue multiple sounds
    * [o] controllable queue
        * [x] TTS captions, while building to resource files
        * [ ] .raw.R.id resources files!
        * [x] it is in a MutableList, ergo, controllable
        * [ ] monitor/parse each event
        * [ ] considerations for length of each event
    * [o] visual representation 
        * [x] captions
        * [ ] seekbar ticks for triggers
* [o] data persistence
    * [ ] orientation changes
    * [ ] write to record, JSON
    * [x] local SQLite Db
    * [ ] preferences?
    
#### Completed
* [x] LatLng to m / feet
    * [x] distance calculations
    * [x] Double for comparisons
* [x] clean-up fragments
* [x] test audio

## Project Structure
This section is meant for anyone trying to navigate this source code project
### MAP
* cianras
    * |database                database classes, Singletons
        * |events              > classes describing distance/time triggers
        * |intervals           > classes describing intervals
        * |location            > classes describing the location
            * -DataLocation    * !!Singleton for all location information!!
        * |races               > components for SQL entries, UI adapters
    * |ui                      > user interface fragments 
        * |home                !! HOME FRAGMENT !! First fragment to load in the MAIN TASK
        * |intervals           > Intervals fragment; in infancy
        * |location            > Map fragment; phase out after dev
        * |minterface          > Minimal fragment; goal is to display very little
        * |progress            > BubbleBar fragment; handling the progress bar is a task
        * |races               > Races Fragment; expandable list of previous races
        * |timer               > Timer Fragment; try to maintain single instance
   * -AudioPipeline            * deprecated; dissolved to Overseer
   * -FragmentRace             * deprecated activity
   * -Main3Activity            * !! MAIN TASK !! Launcher.rev3 !! Handles all fragments
   * -MinimalActivity          * deprecated activity
   * -Mobilization             * deprecated location class
   * -Overseer                 * Co-ordinator Fragment for Events (sounds, captions)
   * -RaceActivity             * deprecated activity
   * -RacesActivity            * deprecated activity

Still learning Markdown, bare with me while I work on formatting the Map ^^
I have highlighted a few important files, and some that can/should be cleaned/disregarded.
Because of the navigation scheme of the side drawer, it can be a chore to add/remove or even
troubleshoot a UI screen. Alas, it was the template project UI that I started with; can be changed
if/when the need changes.

Main3Activity loads all other fragments and controls the draw to the device. 
DataLocation, at the moment, is the main Singleton for the running variables and required constants.
Overseer runs async with the MainActivity and parses location/time for events

My Links
https://codealike.com/facts/Sabaoth87
https://bitbucket.org/sabaoth/cianras-alpha/src/master/
https://github.com/sabaoth87